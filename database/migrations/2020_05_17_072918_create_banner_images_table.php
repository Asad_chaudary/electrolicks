<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ht_banner_image')->nullable();
            $table->string('hb_banner_image_1')->nullable();
            $table->string('hb_banner_image_2')->nullable();
            $table->string('hr_banner_image_1')->nullable();
            $table->string('hr_banner_image_2')->nullable();
            $table->string('hr_banner_image_3')->nullable();
            $table->string('center_banner_image')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_images');
    }
}
