<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoryTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategory_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sct_name');
            $table->string('sct_icon')->nullable();
            $table->string('image_name')->nullable();
            $table->text('description')->nullable();
            $table->boolean('status')->default(1);
            $table->bigInteger('cat_id')->unsigned();
            $table->foreign('cat_id')->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategory_types');
    }
}
