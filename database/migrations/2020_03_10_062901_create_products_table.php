<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->default(0);
            $table->string('title');
            $table->string('slug');
            $table->text('description')->nullable();
            $table->text('featured_image')->nullable();
            $table->float('price');
            $table->unsignedInteger('discount')->nullable();
            $table->string('star_rating')->nullable();
            $table->boolean('product_on_sale')->default(0);
            $table->boolean('status')->default(1);
            $table->bigInteger('subc_id')->default(0);
            $table->bigInteger('brand_id')->unsigned();
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
