<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\SubcategoryType;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $faker = Faker\Factory::create();

        $category = new Category();
	    for ($i = 0; $i < 10; $i++) {
	    	$category->c_name = $faker->title;
	    	$category->user_id = 1;
	    	$category->description = $faker->text;
	   		$category->save();
	    }

	    $categories = Category::all();

	    $subcategory_type = new SubcategoryType();
	    foreach ($categories as $key => $value) {
	    	$subcategory_type->sct_name = $faker->title;
	    	$subcategory_type->description = $faker->text;
	    	$subcategory_type->cat_id = $value->id;
	    	$subcategory_type->save();
	    }
	    
	    $subcategory_type = SubcategoryType::all();

	    $subcategory = new Subcategory();
	    foreach ($subcategory_type as $key => $value) {
	    	$subcategory->sc_name = $faker->title;
	    	$subcategory->description = $faker->text;
	    	$subcategory->sct_id = $value->id;
	    	$subcategory->save();
	    }
    }



}
