<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BannerImages;
use App\Models\HomeSlider;
use App\Models\FeaturedCategory;
use App\Models\FeaturedProduct;
use App\Models\FeaturedSubcategory;
use App\Models\FeaturedSubcategorytype;
use App\Models\HotCategory;
use App\Models\HotProduct;
use App\Models\HotSubcategory;
use App\Models\HotSubcategorytype;
use App\Models\NewCategory;
use App\Models\NewProduct;
use App\Models\NewSubcategory;
use App\Models\NewSubcategorytype;
use App\Product;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2 ){
        return route('admin.index');
        }elseif(auth()->user()->role_id == 3 ){
        return redirect('/');
        }

    }

    public function adminDashboard(){
            return view('dashboard.admin');    
        }


    public function mainPage(){
        $banner_images = BannerImages::where('status',1)->first();
        $data['ht_banner_image'] = isset($banner_images) && !empty($banner_images->ht_banner_image) ? 'uploads/banner_images'.'/'.$banner_images->ht_banner_image : 'image/demo/cms/v2-banner2.jpg';
        $data['hb_banner_image_1'] = isset($banner_images) && !empty($banner_images->hb_banner_image_1) ? 'uploads/banner_images'.'/'.$banner_images->hb_banner_image_1 : 'image/demo/cms/v2-banner3.jpg';
        $data['hb_banner_image_2'] = isset($banner_images) && !empty($banner_images->hb_banner_image_2) ? 'uploads/banner_images'.'/'.$banner_images->hb_banner_image_2 : 'image/demo/cms/v2-banner4.jpg';
        $data['hr_banner_image_1'] = isset($banner_images) && !empty($banner_images->hr_banner_image_1) ? 'uploads/banner_images'.'/'.$banner_images->hr_banner_image_1 : 'image/demo/cms/v2-banner1.jpg';
        $data['hr_banner_image_2'] = isset($banner_images) && !empty($banner_images->hr_banner_image_2) ? 'uploads/banner_images'.'/'.$banner_images->hr_banner_image_2 : 'image/demo/cms/v2-banner-right.jpg';
        $data['hr_banner_image_3'] = isset($banner_images) && !empty($banner_images->hr_banner_image_3) ? 'uploads/banner_images'.'/'.$banner_images->hr_banner_image_3 : 'image/demo/cms/v3-banner-home-left.jpg';
        $data['center_banner_image'] = isset($banner_images) && !empty($banner_images->center_banner_image) ? 'uploads/banner_images'.'/'.$banner_images->center_banner_image : 'image/demo/cms/v2-banner-home.jpg';

        $subcategory = FeaturedSubcategory::with('subcategory')->take(3)->get();

        $sales_products = Product::where('product_on_sale',1)->where('status',1)->take(4)->get();
        $home_slider = HomeSlider::where('status',1)->get();

        $hot_categories = HotCategory::with(['category' => function($q){
            $q->select('id','c_name','image_name')->get()->toArray();
        }])->get()->toArray();
        // dd(collect($hot_categories));
        return view('main',compact('hot_categories','data','home_slider','sales_products','subcategory'));
    }

    public function about_us(){
        return view('static_pages.about_us');
    }

    public function login(){

        if( auth()->user()  && (auth()->user()->role_id == 1 || auth()->user()->role_id == 2)  )
            return redirect('/admin');
        else
            return view('logins.admin');
        
    }

    public function showDashbaord(){
        if(auth()){
        
            if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2  )
            return view('dashboard.admin'); 
            elseif(auth()->user()->role_id == 3)
            return redirect('/');
            else
            return 'Not Found';
            
        }
    }

    public function customerDashboard(){
        return view('dashboard.customer');
    }
}
