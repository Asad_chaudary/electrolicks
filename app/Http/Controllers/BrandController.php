<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
        // dd($brands);
        return view('brands.index' , compact('brands') );
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brands.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'sometimes',
            'image_name' => 'required'
         ]);

        $brands = new Brand();
        $brands->user_id = auth()->user()->id;
        $brands->name = $request->name;
        $brands->status = $request->status;
        $brands->description = $request->description;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $folder = 'brands';
            $brands->image_name = saveImages($file, $folder);
        }
        $brands->save();
 
         if($brands){
             return redirect('/admin/brands')->with('success' , 'Vendor added successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('brands.edit' , compact('brand') );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'sometimes',
         ]);

        $brands = Brand::find($id);
        $brands->user_id = auth()->user()->id;
        $brands->name = $request->name;
        $brands->status = $request->status;
        $brands->description = $request->description;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $folder = 'brands';
            $old_file_name = $brands->image_name;
            $brands->image_name = saveImages($file, $folder,$old_file_name);
        }

         if($brands){
             return redirect('/admin/brands')->with('success' , 'Vendor updated successfully');
         }else{
             return redirect()->back()->with('error' , 'Some problem occoured');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        $brand->delete();
        return redirect()->back()->with('success' , 'Vendor deleted successfully');
    }

    public function change_status($id){
        
        $brands = Brand::find($id);
        $brands->status = $brands->status == 1 ? 0 : 1;
        $brands->save();
        if($brands){
            return redirect()->route('admin.brands.index')->with('success' , 'Status updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }
}
