<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SubcategoryType;
use App\Models\Category;

class SubcategoryTypeController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategory_type = SubcategoryType::paginate(20);
        return view('admin.category.subcategory_type.index',compact('subcategory_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('admin.category.subcategory_type.form',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'sct_name' => 'required',
           'cat_id' => 'required',
           'description' => 'sometimes',
        ]);

        
        $subcategory_type = new SubcategoryType();

        $subcategory_type->cat_id = $request->cat_id;
        $subcategory_type->sct_name = $request->sct_name;
        $subcategory_type->sct_icon = $request->sct_icon;
        $subcategory_type->status = $request->status;
        $subcategory_type->description = $request->description;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $folder = 'subcategory_type';
            $subcategory_type->image_name = saveImages($file, $folder);
        }

        if ($request->file('sc_icon')) {
            $file = $request->sc_icon;
            $folder = 'subcategory_type';
            $subcategory->sc_icon = saveIcons($file, $folder);
        }

        $subcategory_type->save();

        if($subcategory_type){
            return redirect()->route('subcategory-type.index')->with('success' , 'Category added successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::all();
        $subcategory_type = SubcategoryType::find($id);
         return view('admin.category.subcategory_type.edit',compact('subcategory_type','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
           'sct_name' => 'required',
           'cat_id' => 'required',
           'description' => 'sometimes',
        ]);

        
        $subcategory_type = SubcategoryType::find($id);
        $subcategory_type->cat_id = $request->cat_id;
        $subcategory_type->sct_name = $request->sct_name;
        $subcategory_type->sct_icon = $request->sct_icon;
        $subcategory_type->status = $request->status;
        $subcategory_type->description = $request->description;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $folder = 'subcategory_type';
            $old_file_name = $subcategory_type->image_name;
            $subcategory_type->image_name = saveImages($file, $folder,$old_file_name);
        }

        if ($request->file('sct_icon')) {
            $file = $request->sct_icon;
            $folder = 'subcategory_type';
            $old_file_name = $subcategory_type->sct_icon;
            $subcategory_type->sct_icon = saveIcons($file, $folder,$old_file_name);
        }
        
        $subcategory_type->save();
        
        if($subcategory_type){
            return redirect()->route('subcategory-type.index')->with('success' , 'Category updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory_type = SubcategoryType::find($id);
        $subcategory_type->delete();
    }

    public function change_status($id){
    
        $subcategory_type = SubcategoryType::find($id);
        $subcategory_type->status = $subcategory_type->status == 1 ? 0 : 1;
        $subcategory_type->save();
        if($subcategory_type){
            return redirect()->route('subcategory-type.index')->with('success' , 'Status updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }
}
