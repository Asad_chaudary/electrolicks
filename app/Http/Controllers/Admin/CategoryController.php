<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Traits\FileUpload;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(20);
        return view('admin.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'c_name' => 'required',
           'description' => 'sometimes',
        ]);

        
        $category = new Category();

        $category->user_id = auth()->user()->id;
        $category->c_name = $request->c_name;
        $category->status = $request->status;
        $category->description = $request->description;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $folder = 'category';
            $category->image_name = saveImages($file, $folder);
        }
        if ($request->file('c_icon')) {
            $file = $request->c_icon;
            $folder = 'category';
            $category->c_icon = saveIcons($file, $folder);
        }

        $category->save();

        if($category){
            return redirect()->route('categories.index')->with('success' , 'Category added successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
         return view('admin.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
           'c_name' => 'required',
           'description' => 'sometimes',
        ]);

        
        $category = Category::find($id);
        $category->user_id = auth()->user()->id;
        $category->c_name = $request->c_name;
        $category->status = $request->status;
        $category->description = $request->description;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $folder = 'category';
            $old_file_name = $category->image_name;
            $category->image_name = saveImages($file, $folder,$old_file_name);
        }

        if ($request->file('c_icon')) {
            $file = $request->c_icon;
            $folder = 'category';
            $old_file_name = $category->c_icon;
            $category->c_icon = saveIcons($file, $folder,$old_file_name);
        }

        $category->save();
        
        if($category){
            return redirect()->route('categories.index')->with('success' , 'Category updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
    }

    public function change_status($id){
    
        $category = Category::find($id);
        $category->status = $category->status == 1 ? 0 : 1;
        $category->save();
        if($category){
            return redirect()->route('categories.index')->with('success' , 'Status updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }
}
