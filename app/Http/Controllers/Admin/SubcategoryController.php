<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subcategory;
use App\Models\SubcategoryType;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategory = Subcategory::paginate(20);
        return view('admin.category.subcategory.index',compact('subcategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subcategory_type = SubcategoryType::all();
        return view('admin.category.subcategory.form',compact('subcategory_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'sc_name' => 'required',
           'sct_id' => 'required',
           'description' => 'sometimes',
        ]);

        
        $subcategory = new Subcategory();

        $subcategory->sct_id = $request->sct_id;
        $subcategory->sc_name = $request->sc_name;
        $subcategory->status = $request->status;
        $subcategory->description = $request->description;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $folder = 'subcategory';
            $subcategory->image_name = saveImages($file, $folder);
        }
        if ($request->file('sc_icon')) {
            $file = $request->sc_icon;
            $folder = 'subcategory';
            $subcategory->sc_icon = saveIcons($file, $folder);
        }
        $subcategory->save();

        if($subcategory){
            return redirect()->route('subcategories.index')->with('success' , 'Category added successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategory_type = SubcategoryType::all();
        $subcategory = Subcategory::find($id);
         return view('admin.category.subcategory.edit',compact('subcategory_type','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
           'sc_name' => 'required',
           'sct_id' => 'required',
           'description' => 'sometimes',
        ]);

        
        $subcategory = Subcategory::find($id);
        $subcategory->sct_id = $request->sct_id;
        $subcategory->sc_name = $request->sc_name;
        $subcategory->status = $request->status;
        $subcategory->description = $request->description;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $folder = 'subcategory';
            $old_file_name = $subcategory->image_name;
            $subcategory->image_name = saveImages($file, $folder,$old_file_name);
        }

        if ($request->file('sc_icon')) {
            $file = $request->sc_icon;
            $folder = 'subcategory';
            $old_file_name = $subcategory->sc_icon;
            $subcategory->sc_icon = saveIcons($file, $folder,$old_file_name);
        }

        $subcategory->save();
        
        if($subcategory){
            return redirect()->route('subcategories.index')->with('success' , 'Category updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = Subcategory::find($id);
        $subcategory->delete();
    }

    public function change_status($id){
    
        $subcategory = Subcategory::find($id);
        $subcategory->status = $subcategory->status == 1 ? 0 : 1;
        $subcategory->save();
        if($subcategory){
            return redirect()->route('subcategories.index')->with('success' , 'Status updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    public function subCategory($id){
        dd("id is = ".$id);
    }
}
