<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    public function privacypolicy()
    {
    	return view('frontend.pages.content.privacy-policy');
    }
    public function terms()
    {
    	return view('frontend.pages.content.terms');
    }
    public function contact()
    {
    	return view('frontend.pages.content.contact');
    }
    public function faqs()
    {
        return view('frontend.pages.content.faqs');
    }
}
