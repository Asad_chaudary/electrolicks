<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BannerImages;
use App\Models\HomeSlider;
use App\Models\FeaturedCategory;
use App\Models\FeaturedProduct;
use App\Models\FeaturedSubcategory;
use App\Models\FeaturedSubcategorytype;
use App\Models\HotCategory;
use App\Models\HotProduct;
use App\Models\HotSubcategory;
use App\Models\HotSubcategorytype;
use App\Models\NewCategory;
use App\Models\NewProduct;
use App\Models\NewSubcategory;
use App\Models\NewSubcategorytype;
use App\Product;

class HomeController extends Controller
{
    
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2 ){
        return route('admin.index');
        }elseif(auth()->user()->role_id == 3 ){
        return redirect('/');
        }

    }

    public function adminDashboard(){
            return view('dashboard.admin');    
        }


    public function mainPage(){
        $banner_images = BannerImages::where('status',1)->first();
        $data['ht_banner_image'] = isset($banner_images) && !empty($banner_images->ht_banner_image) ? 'uploads/banner_images'.'/'.$banner_images->ht_banner_image : 'image/demo/cms/v2-banner2.jpg';
        $data['hb_banner_image_1'] = isset($banner_images) && !empty($banner_images->hb_banner_image_1) ? 'uploads/banner_images'.'/'.$banner_images->hb_banner_image_1 : 'image/demo/cms/v2-banner3.jpg';
        $data['hb_banner_image_2'] = isset($banner_images) && !empty($banner_images->hb_banner_image_2) ? 'uploads/banner_images'.'/'.$banner_images->hb_banner_image_2 : 'image/demo/cms/v2-banner4.jpg';
        $data['hr_banner_image_1'] = isset($banner_images) && !empty($banner_images->hr_banner_image_1) ? 'uploads/banner_images'.'/'.$banner_images->hr_banner_image_1 : 'image/demo/cms/v2-banner1.jpg';
        $data['hr_banner_image_2'] = isset($banner_images) && !empty($banner_images->hr_banner_image_2) ? 'uploads/banner_images'.'/'.$banner_images->hr_banner_image_2 : 'image/demo/cms/v2-banner-right.jpg';
        $data['hr_banner_image_3'] = isset($banner_images) && !empty($banner_images->hr_banner_image_3) ? 'uploads/banner_images'.'/'.$banner_images->hr_banner_image_3 : 'image/demo/cms/v3-banner-home-left.jpg';
        $data['center_banner_image'] = isset($banner_images) && !empty($banner_images->center_banner_image) ? 'uploads/banner_images'.'/'.$banner_images->center_banner_image : 'image/demo/cms/v2-banner-home.jpg';

        $subcategory = FeaturedSubcategory::with('subcategory')->take(3)->get();

        $sales_products = Product::where('product_on_sale',1)->where('status',1)->take(4)->get();
        $home_slider = HomeSlider::where('status',1)->get();

        $hot_categories = HotCategory::with(['category' => function($q){
            $q->select('id','c_name','image_name')->get()->toArray();
        }])->get()->toArray();
        $featured_products = FeaturedSubcategory::with(['subcategory','subcategory.product'])->get()->toArray();

        // dd(collect($featured_products));
        return view('frontend.pages.home.index',compact('featured_products','hot_categories','data','home_slider','sales_products','subcategory'));
    }

    public function about_us(){
        return view('static_pages.about_us');
    }


    public function login(){

        if( auth()->user()  && (auth()->user()->role_id == 1 || auth()->user()->role_id == 2)  )
            return redirect('/admin');
        else
            return view('logins.admin');
        
    }

    public function showDashbaord(){
        if(auth()){
        
            if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2  )
            return view('dashboard.admin'); 
            elseif(auth()->user()->role_id == 3)
            return redirect('/');
            else
            return 'Not Found';
            
        }
    }

    public function customerDashboard(){
        return view('dashboard.customer');
    }

    public function featured_products($id){

        $data = '
                            <!--Begin Items-->
                            <div class="ltabs-items items-category-18 grid ltabs-items-selected" data-total="10">
                                <div class="ltabs-items-inner ltabs-slider  owl2-carousel owl2-theme owl2-loaded">
                                
                                <div class="owl2-stage-outer"><div class="owl2-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2100px;"><div class="owl2-item active" style="width: 270px; margin-right: 30px;"><div class="ltabs-item product-layout">
                                        <div class="product-item-container">
                                            <div class="left-block">
                                                <div class="product-image-container second_img ">
                                                    <img src="image/demo/shop/resize/J9-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                                    <img src="image/demo/shop/resize/J5-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                                </div>
                                                <!--Sale Label-->
                                                <span class="label label-sale">-15%</span>
                                                <!--full quick view block-->
                                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                                <!--end full quick view block-->
                                            </div>
                                            <div class="right-block">
                                                <div class="caption">
                                                    <h4><a href="product.html">Cupim Bris</a></h4>      
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        </div>
                                                    </div>
                                                                        
                                                    <div class="price">
                                                        <span class="price-new">$50.00</span> 
                                                        <span class="price-old">$62.00</span>        
                                                    </div>
                                                </div>
                                                
                                                  <div class="button-group">
                                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add("42", "1");" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add("42");" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add("42");" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                                  </div>
                                            </div><!-- right block -->
                                        </div>
                                    </div></div><div class="owl2-item active" style="width: 270px; margin-right: 30px;"><div class="ltabs-item product-layout">
                                        <div class="product-item-container">
                                            <div class="left-block">
                                                <div class="product-image-container second_img ">
                                                    <img src="image/demo/shop/resize/m1-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                                    <img src="image/demo/shop/resize/m3-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                                </div>
                                                <!--New Label-->
                                                <span class="label label-new">New</span>
                                                <!--full quick view block-->
                                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                                <!--end full quick view block-->
                                            </div>
                                            <div class="right-block">
                                                <div class="caption">
                                                    <h4><a href="product.html">Cisi Chicken </a></h4>       
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        </div>
                                                    </div>
                                                                        
                                                    <div class="price">
                                                        <span class="price-new">$59.00</span> 
                                                    </div>
                                                </div>
                                                
                                                  <div class="button-group">
                                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add("42", "1");" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add("42");" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add("42");" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                                  </div>
                                            </div><!-- right block -->
                                        </div>
                                    </div></div><div class="owl2-item active" style="width: 270px; margin-right: 30px;"><div class="ltabs-item product-layout">
                                        <div class="product-item-container">
                                            <div class="left-block">
                                                <div class="product-image-container second_img ">
                                                    <img src="image/demo/shop/resize/B10-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                                    <img src="image/demo/shop/resize/B9-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                                </div>
                                                <!--Sale Label-->
                                                <!--full quick view block-->
                                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                                <!--end full quick view block-->
                                            </div>
                                            <div class="right-block">
                                                <div class="caption">
                                                    <h4><a href="product.html">Bint Beef</a></h4>       
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        </div>
                                                    </div>
                                                                        
                                                    <div class="price">
                                                        <span class="price-new">$97.00</span> 
                                                    </div>
                                                </div>
                                                
                                                  <div class="button-group">
                                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add("42", "1");" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add("42");" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add("42");" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                                  </div>
                                            </div><!-- right block -->
                                        </div>
                                    </div></div><div class="owl2-item" style="width: 270px; margin-right: 30px;"><div class="ltabs-item product-layout">
                                        <div class="product-item-container">
                                            <div class="left-block">
                                                <div class="product-image-container second_img ">
                                                    <img src="image/demo/shop/resize/w1-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                                    <img src="image/demo/shop/resize/w10-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                                </div>
                                                
                                                <!--full quick view block-->
                                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                                <!--end full quick view block-->
                                            </div>
                                            <div class="right-block">
                                                <div class="caption">
                                                    <h4><a href="product.html">Dail Lulpa</a></h4>      
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        </div>
                                                    </div>
                                                                        
                                                    <div class="price">
                                                        <span class="price-new">$97.00</span> 
                                                    </div>
                                                </div>
                                                
                                                  <div class="button-group">
                                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add("42", "1");" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add("42");" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add("42");" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                                  </div>
                                            </div><!-- right block -->
                                        </div>
                                    </div></div><div class="owl2-item" style="width: 270px; margin-right: 30px;"><div class="ltabs-item product-layout">
                                        <div class="product-item-container">
                                            <div class="left-block">
                                                <div class="product-image-container second_img ">
                                                    <img src="image/demo/shop/resize/B5-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                                    <img src="image/demo/shop/resize/B10-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                                </div>
                                                
                                                <!--full quick view block-->
                                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                                <!--end full quick view block-->
                                            </div>
                                            <div class="right-block">
                                                <div class="caption">
                                                    <h4><a href="product.html">Et Spare</a></h4>        
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        </div>
                                                    </div>
                                                                        
                                                    <div class="price">
                                                        <span class="price-new">$65.00</span> 
                                                    </div>
                                                </div>
                                                
                                                  <div class="button-group">
                                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add("42", "1");" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add("42");" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add("42");" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                                  </div>
                                            </div><!-- right block -->
                                        </div>
                                    </div></div><div class="owl2-item" style="width: 270px; margin-right: 30px;"><div class="ltabs-item product-layout">
                                        <div class="product-item-container">
                                            <div class="left-block">
                                                <div class="product-image-container second_img ">
                                                    <img src="image/demo/shop/resize/J5-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                                    <img src="image/demo/shop/resize/J9-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                                </div>
                                                <!--Sale Label-->
                                                <span class="label label-sale">-15%</span>
                                                <!--full quick view block-->
                                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                                <!--end full quick view block-->
                                            </div>
                                            <div class="right-block">
                                                <div class="caption">
                                                    <h4><a href="product.html">Cupim Bris</a></h4>      
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        </div>
                                                    </div>
                                                                        
                                                    <div class="price">
                                                        <span class="price-new">$50.00</span> 
                                                        <span class="price-old">$62.00</span>        
                                                    </div>
                                                </div>
                                                
                                                  <div class="button-group">
                                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add("42", "1");" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add("42");" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add("42");" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                                  </div>
                                            </div><!-- right block -->
                                        </div>
                                    </div></div><div class="owl2-item" style="width: 270px; margin-right: 30px;"><div class="ltabs-item product-layout">
                                        <div class="product-item-container">
                                            <div class="left-block">
                                                <div class="product-image-container second_img ">
                                                    <img src="image/demo/shop/resize/e11-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                                    <img src="image/demo/shop/resize/E3-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                                </div>
                                                <!--Sale Label-->
                                                <span class="label label-sale">-15%</span>
                                                <!--full quick view block-->
                                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                                <!--end full quick view block-->
                                            </div>
                                            <div class="right-block">
                                                <div class="caption">
                                                    <h4><a href="product.html">Apple Cinema 30"</a></h4>        
                                                    <div class="ratings">
                                                        <div class="rating-box">
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        </div>
                                                    </div>
                                                                        
                                                    <div class="price">
                                                        <span class="price-new">$50.00</span> 
                                                        <span class="price-old">$62.00</span>        
                                                    </div>
                                                </div>
                                                
                                                  <div class="button-group">
                                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add("42", "1");" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add("42");" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add("42");" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                                  </div>
                                            </div><!-- right block -->
                                        </div>
                                    </div></div></div></div><div class="owl2-controls"><div class="owl2-nav"><div class="owl2-prev" style=""></div><div class="owl2-next" style=""></div></div><div class="owl2-dots" style="display: none;"></div></div></div>
                                
                            </div>
                            <div class="ltabs-items items-category-18 grid ltabs-items-loaded" data-total="11">
                                <div class="ltabs-items-container">
    <div class="ltabs-items items-category-18 grid ltabs-items-selected ltabs-items-loaded" data-total="10">
        <div class="ltabs-items-inner ltabs-slider  owl2-carousel owl2-theme owl2-loaded owl2-hidden">
            
        <div class="owl2-stage-outer"><div class="owl2-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 43.333px;"><div class="owl2-item active" style="width: 13.333px; margin-right: 30px;"><div class="owl2-stage-outer">
                <div class="owl2-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2100px;">
                    {{--  products listing start here  --}}
                    <div class="owl2-item active" style="width: 270px; margin-right: 30px;">
                        <div class="ltabs-item product-layout">
                            <div class="product-item-container">
                            <div class="left-block">
                                <div class="product-image-container second_img ">
                                    <img src="image/demo/shop/resize/J9-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                    <img src="image/demo/shop/resize/J5-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                </div>
                                <!--Sale Label-->
                                <span class="label label-sale">-15%</span>
                                <!--full quick view block-->
                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                <!--end full quick view block-->
                            </div>
                            <div class="right-block">
                                <div class="caption">
                                    <h4><a href="product.html">Cupim Bris</a></h4>      
                                    <div class="ratings">
                                        <div class="rating-box">
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        </div>
                                    </div>
                                                        
                                    <div class="price">
                                        <span class="price-new">$50.00</span> 
                                        <span class="price-old">$62.00</span>        
                                    </div>
                                </div>
                                  <div class="button-group">
                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add(" 42",="" "1");"="" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add(" 42");"="" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add(" 42");"="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                  </div>
                            </div><!-- right block -->
                            </div>
                        </div>
                    </div>
                    <div class="owl2-item " style="width: 270px; margin-right: 30px;">
                        <div class="ltabs-item product-layout">
                            <div class="product-item-container">
                            <div class="left-block">
                                <div class="product-image-container second_img ">
                                    <img src="image/demo/shop/resize/J9-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                    <img src="image/demo/shop/resize/J5-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                </div>
                                <!--Sale Label-->
                                <span class="label label-sale">-15%</span>
                                <!--full quick view block-->
                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                <!--end full quick view block-->
                            </div>
                            <div class="right-block">
                                <div class="caption">
                                    <h4><a href="product.html">Cupim Bris</a></h4>      
                                    <div class="ratings">
                                        <div class="rating-box">
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        </div>
                                    </div>
                                                        
                                    <div class="price">
                                        <span class="price-new">$50.00</span> 
                                        <span class="price-old">$62.00</span>        
                                    </div>
                                </div>
                                  <div class="button-group">
                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add(" 42",="" "1");"="" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add(" 42");"="" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add(" 42");"="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                  </div>
                            </div><!-- right block -->
                            </div>
                        </div>
                    </div>
                    <div class="owl2-item " style="width: 270px; margin-right: 30px;">
                        <div class="ltabs-item product-layout">
                            <div class="product-item-container">
                            <div class="left-block">
                                <div class="product-image-container second_img ">
                                    <img src="image/demo/shop/resize/J9-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                    <img src="image/demo/shop/resize/J5-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                </div>
                                <!--Sale Label-->
                                <span class="label label-sale">-15%</span>
                                <!--full quick view block-->
                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                <!--end full quick view block-->
                            </div>
                            <div class="right-block">
                                <div class="caption">
                                    <h4><a href="product.html">Cupim Bris</a></h4>      
                                    <div class="ratings">
                                        <div class="rating-box">
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        </div>
                                    </div>
                                                        
                                    <div class="price">
                                        <span class="price-new">$50.00</span> 
                                        <span class="price-old">$62.00</span>        
                                    </div>
                                </div>
                                  <div class="button-group">
                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add(" 42",="" "1");"="" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add(" 42");"="" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add(" 42");"="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                  </div>
                            </div><!-- right block -->
                            </div>
                        </div>
                    </div>

                    {{--  end products section here s --}}
                </div>
            </div></div></div></div><div class="owl2-controls"><div class="owl2-nav"><div class="owl2-prev" style=""></div><div class="owl2-next" style=""></div></div><div class="owl2-dots" style="display: none;"></div></div></div>
    </div>
</div>
                                
                            </div>
                            <div class="ltabs-items  items-category-25 grid ltabs-items-loaded" data-total="11">
                                <div class="ltabs-items-container">
    <div class="ltabs-items items-category-18 grid ltabs-items-selected ltabs-items-loaded" data-total="10">
        <div class="ltabs-items-inner ltabs-slider  owl2-carousel owl2-theme owl2-loaded owl2-hidden">
            
        <div class="owl2-stage-outer"><div class="owl2-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 43.333px;"><div class="owl2-item active" style="width: 13.333px; margin-right: 30px;"><div class="owl2-stage-outer">
                <div class="owl2-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2100px;">
                    {{--  products listing start here  --}}
                    <div class="owl2-item active" style="width: 270px; margin-right: 30px;">
                        <div class="ltabs-item product-layout">
                            <div class="product-item-container">
                            <div class="left-block">
                                <div class="product-image-container second_img ">
                                    <img src="image/demo/shop/resize/J9-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                    <img src="image/demo/shop/resize/J5-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                </div>
                                <!--Sale Label-->
                                <span class="label label-sale">-15%</span>
                                <!--full quick view block-->
                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                <!--end full quick view block-->
                            </div>
                            <div class="right-block">
                                <div class="caption">
                                    <h4><a href="product.html">Cupim Bris</a></h4>      
                                    <div class="ratings">
                                        <div class="rating-box">
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        </div>
                                    </div>
                                                        
                                    <div class="price">
                                        <span class="price-new">$50.00</span> 
                                        <span class="price-old">$62.00</span>        
                                    </div>
                                </div>
                                  <div class="button-group">
                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add(" 42",="" "1");"="" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add(" 42");"="" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add(" 42");"="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                  </div>
                            </div><!-- right block -->
                            </div>
                        </div>
                    </div>
                    <div class="owl2-item " style="width: 270px; margin-right: 30px;">
                        <div class="ltabs-item product-layout">
                            <div class="product-item-container">
                            <div class="left-block">
                                <div class="product-image-container second_img ">
                                    <img src="image/demo/shop/resize/J9-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                    <img src="image/demo/shop/resize/J5-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                </div>
                                <!--Sale Label-->
                                <span class="label label-sale">-15%</span>
                                <!--full quick view block-->
                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                <!--end full quick view block-->
                            </div>
                            <div class="right-block">
                                <div class="caption">
                                    <h4><a href="product.html">Cupim Bris</a></h4>      
                                    <div class="ratings">
                                        <div class="rating-box">
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        </div>
                                    </div>
                                                        
                                    <div class="price">
                                        <span class="price-new">$50.00</span> 
                                        <span class="price-old">$62.00</span>        
                                    </div>
                                </div>
                                  <div class="button-group">
                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add(" 42",="" "1");"="" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add(" 42");"="" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add(" 42");"="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                  </div>
                            </div><!-- right block -->
                            </div>
                        </div>
                    </div>
                    <div class="owl2-item " style="width: 270px; margin-right: 30px;">
                        <div class="ltabs-item product-layout">
                            <div class="product-item-container">
                            <div class="left-block">
                                <div class="product-image-container second_img ">
                                    <img src="image/demo/shop/resize/J9-270x270.jpg" alt="Apple Cinema 30&quot;" class="img-responsive">
                                    <img src="image/demo/shop/resize/J5-270x270.jpg" alt="Apple Cinema 30&quot;" class="img_0 img-responsive">
                                </div>
                                <!--Sale Label-->
                                <span class="label label-sale">-15%</span>
                                <!--full quick view block-->
                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe" href="quickview.html">  Quickview</a>
                                <!--end full quick view block-->
                            </div>
                            <div class="right-block">
                                <div class="caption">
                                    <h4><a href="product.html">Cupim Bris</a></h4>      
                                    <div class="ratings">
                                        <div class="rating-box">
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        </div>
                                    </div>
                                                        
                                    <div class="price">
                                        <span class="price-new">$50.00</span> 
                                        <span class="price-old">$62.00</span>        
                                    </div>
                                </div>
                                  <div class="button-group">
                                    <button class="addToCart" type="button" data-toggle="tooltip" title="" onclick="cart.add(" 42",="" "1");"="" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                    <button class="wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add(" 42");"="" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>
                                    <button class="compare" type="button" data-toggle="tooltip" title="" onclick="compare.add(" 42");"="" data-original-title="Compare this Product"><i class="fa fa-exchange"></i></button>
                                  </div>
                            </div><!-- right block -->
                            </div>
                        </div>
                    </div>

                    {{--  end products section here s --}}
                </div>
            </div></div></div></div><div class="owl2-controls"><div class="owl2-nav"><div class="owl2-prev" style=""></div><div class="owl2-next" style=""></div></div><div class="owl2-dots" style="display: none;"></div></div></div>
    </div>
</div>
                            </div>
                        ';

return $data;


    }
}

                            


