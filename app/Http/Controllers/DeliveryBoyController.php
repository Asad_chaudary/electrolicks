<?php

namespace App\Http\Controllers;

use App\Delivery_Boy;
use Illuminate\Http\Request;

class DeliveryBoyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boys = Delivery_Boy::all();
        return view('delivery_boy.index' , compact('boys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('delivery_boy.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        

        $request->validate([
            "name" => "required",
            "email" => "sometimes",
            "phone" => "sometimes",
            "city" => "sometimes",
            "address" => "sometimes",
        ]);

        $boy = Delivery_Boy::create([
            "name" => $request->name, 
            "email" => $request->email, 
            "phone" => $request->phone, 
            "city" => $request->city, 
            "address" => $request->address, 
            "user_id" => auth()->user()->id , 
        ]);

        if($boy){
            return redirect('/dashboard/delivery_boys')->with('success' , 'Delivery Boy created successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Delivery_Boy  $delivery_Boy
     * @return \Illuminate\Http\Response
     */
    public function show(Delivery_Boy $delivery_Boy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Delivery_Boy  $delivery_Boy
     * @return \Illuminate\Http\Response
     */
    public function edit($boy_id)
    {
        $boy = Delivery_Boy::findOrFail($boy_id);
        return view('delivery_boy.edit' , compact('boy') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Delivery_Boy  $delivery_Boy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $delivery_boy_id)
    {
        $request->validate([
            "name" => "required",
            "email" => "sometimes",
            "phone" => "sometimes",
            "city" => "sometimes",
            "address" => "sometimes",
        ]);

        $boy = Delivery_Boy::findOrFail($delivery_boy_id);
        $boy->name = $request->name; 
        $boy->email = $request->email; 
        $boy->phone = $request->phone; 
        $boy->city = $request->city; 
        $boy->address = $request->address; 
        $boy->save(); 

        if($boy->save()){
            return redirect('/dashboard/delivery_boys')->with('success' , 'Delivery Boy updated successfully');
        }else{
            return redirect()->back()->with('error' , 'Some problem occoured');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Delivery_Boy  $delivery_Boy
     * @return \Illuminate\Http\Response
     */
    public function destroy($delivery_boy_id)
    {
        $boy  = Delivery_Boy::findOrFail($delivery_boy_id);
        $boy->delete();
        return redirect()->back()->with('success' , 'Delivery Boy deleted successfully');

    }
}
