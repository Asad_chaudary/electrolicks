<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
 	protected $primaryKey = 'id';

    protected $guarded = [];

    public function brand_name(){
    	return $this->belongsTo('App\Models\Brand','brand_id')->select('name');
    }
    public function subcategory_name(){
    	return $this->belongsTo('App\Models\Subcategory','subc_id')->select('title');
    }

    public function subcategory(){
    	return $this->belongsTo('App\Models\Subcategory','subc_id');
    }
}
