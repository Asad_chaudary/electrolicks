<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotCategory extends Model
{
    protected $table = 'hot_categories';
 	protected $primaryKey = 'id';

    protected $guarded = [];

    public function category(){
    	return $this->belongsTo(Category::class,'category_id');
    }
}
