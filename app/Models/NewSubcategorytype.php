<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewSubcategorytype extends Model
{
    
    protected $table = 'new_subcategorytypes';
 	protected $primaryKey = 'id';

    protected $guarded = [];

    public function subcategory_type(){
    	return $this->belongsTo(SubcategoryType::class,'subcategory_type_id');
    }

}
