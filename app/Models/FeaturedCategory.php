<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeaturedCategory extends Model
{
    
    protected $table = 'featured_categories';
 	protected $primaryKey = 'id';

    protected $guarded = [];

    public function category(){
    	return $this->belongsTo(Category::class,'category_id');
    }
}
