<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
 	protected $primaryKey = 'id';

    protected $guarded = [];

    public function product(){
    	return $this->hasMany('App\Product','id');
    }
}
