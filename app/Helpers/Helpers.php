<?php
  use App\Models\Category;
  use App\Models\FeaturedProduct;
  
  if(!function_exists('category')){
    function category(){
      $category = Category::with(['subcategory_type','subcategory_type.subcategory'])->get();
      return $category;
    }
  }
  // featured products 
  if(!function_exists('featured_products')){
    function featured_products($subc_id){
      $featured_products = FeaturedProduct::with(['product' => function($q) use($subc_id){
        $q->where('subc_id',$subc_id)->get();
      } ])->get();

      return $featured_products;
    }
  }

    /**
     * Image upload trait used in controllers to upload files
     */
  
  if(!function_exists('saveImages')){
    function saveImages($file, $folder,$old_file_name = null)
    {
        $destinationPath = '/uploads/'.$folder;

        $file_name = time().'-'.$file->getClientOriginalName();

        $image = Image::make($file);
        
        $path = public_path() . $destinationPath;
        if(!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $image->resize(1920, 1080, function ($constraint) {
              $constraint->aspectRatio();
        })->save(public_path() . $destinationPath.DIRECTORY_SEPARATOR. $file_name);
            
        $path = public_path() . $destinationPath .DIRECTORY_SEPARATOR.'thumbnails';
        if(!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $image->resize(350, 240, function ($constraint) {
              $constraint->aspectRatio();
        })->save(public_path() . $destinationPath .DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR. $file_name);

          if(!is_null($old_file_name)){
              $file_path = public_path() . $destinationPath.DIRECTORY_SEPARATOR. $old_file_name;
                  if (is_file($file_path)) {
                      unlink($file_path);
                  }
              $file_path = public_path() . $destinationPath .DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR. $old_file_name;
                  if (is_file($file_path)) {
                      unlink($file_path);
                  }
          }

        return $file_name;
    }
  }
    

if(!function_exists('saveIcons')){
    function saveIcons($file, $folder,$old_file_name = null)
    {
        $destinationPath = '/uploads/'.$folder;

        $file_name = time().'-'.$file->getClientOriginalName();

        $image = Image::make($file);
        
        $path = public_path() . $destinationPath .DIRECTORY_SEPARATOR.'icons';
        if(!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $image->resize(40, 40, function ($constraint) {
              $constraint->aspectRatio();
        })->save(public_path() . $destinationPath .DIRECTORY_SEPARATOR.'icons'.DIRECTORY_SEPARATOR. $file_name);

          if(!is_null($old_file_name)){
              $file_path = public_path() . $destinationPath .DIRECTORY_SEPARATOR.'icons'.DIRECTORY_SEPARATOR. $old_file_name;
                  if (is_file($file_path)) {
                      unlink($file_path);
                  }
          }

        return $file_name;
    }
  }