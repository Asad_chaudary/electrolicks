@extends('layouts.admin')


@section('content')


<div class="row">


    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Coupen Boys</h3>
        <a href="{{route('admin.coupens.create')}}" class="btn btn-success float-right" > <i class="fas fa-plus"></i> Create Coupen</a>
      </div> 
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <thead>                  
            <tr>
              <th >#</th>
              <th>Name</th>
              <th>Discount</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Created</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($coupens as $k => $coupen)
                  
            <tr>
              <td>{{$k+1}}.</td>
              <td>{{$coupen->name}}</td>
              <td>{{$coupen->discount}}</td>
              <td>{{$coupen->start_date->toDateString() }} </td>
              <td>{{$coupen->end_date->toDateString() }} </td>
              <td> {{$coupen->created_at->toDateString() }} </td>

              <td> <a onclick="deleteCoupen({{$coupen->id}})" href="javascript:;" class="text-danger"> <i class="fas fa-trash"></i> </a> 
               
              <a href="{{route('admin.coupens.edit' , $coupen->id)}}" class="text-primary"> <i class="fas fa-edit"></i> </a>

              <form id="delete-boy-{{$coupen->id}}" action="{{route('admin.coupens.destroy' , $coupen->id)}}" method="POST" style="display: none" >
                @csrf 
                @method('DELETE')
              </form>

              </td>
            </tr>
            @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
  <script>
    function deleteCoupen(id){

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          document.getElementById('delete-boy-'+id).submit();
        }
      });

    }   
  </script>
@endsection