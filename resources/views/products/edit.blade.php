@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="{{asset('/admin-plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('/admin-plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

@endsection

@section('content')


<div class="row">

    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

    <form action="{{route('admin.product.update' , $product->id)}}" method="POST" style=" width:100%; display:flex;" enctype="multipart/form-data">
      @csrf
      @method('PATCH')
  
    <div class="col-md-8" style="padding:10px; padding-left:30px;" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Update Product</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >

        <div class="form-group">
              <label class="col-form-label">Sub category</label>
              <select class="custom-select" name="subc_id" id="subc_id" value="{{old('subc_id')}}" required="">
              <option value="">Select Subcategory</option>
              @if(isset($subcategory) && !empty($subcategory))
                @foreach($subcategory as $key => $value)
                  <option value="{{ $value->id }}">{{ $value->sc_name }}</option>
                @endforeach
              @endif
            </select>
          </div>
        <div class="form-group">
              <label class="col-form-label">Brands</label>
              <select class="custom-select" name="brand_id" id="brand_id" value="{{old('brand_id')}}" required="">
              <option value="">Select Brand</option>
              @if(isset($brands) && !empty($brands))
                @foreach($brands as $key => $value)
                  <option value="{{ $value->id }}">{{ $value->name }}</option>
                @endforeach
              @endif
            </select>
          </div>

          <div class="form-group">
              <label class="col-form-label">Title</label>
              <input type="text" class="form-control" name="title" placeholder="product title" value="{{$product->title}}" required id="post_title" />
              <p class="small">{{config('app.url')}}/<span id="url">{{$product->slug}}</span></p>
              <input type="hidden" value="{{$product->slug}}" name="slug" id="slug" />
          </div>
          <div class="form-group">
              <label class="col-form-label">Image</label>
              <input type="file" class="form-control" name="featured_image" placeholder="product featured_imag" value="{{old('featured_imag')}}" id="post_title" />
          </div>

          <div class="form-group">
              <label class="col-form-label">On Sale</label>
              <select class="custom-select" name="product_on_sale" id="product_on_sale" value="{{old('product_on_sale')}}" required="">
              <option value="">Select On Sale</option>
              <option value="1">Yes</option>
              <option value="0">No</option>
            </select>
          </div>
          <div class="form-group">
              <label class="col-form-label">Status</label>
              <select class="custom-select" name="status" id="status" value="{{old('status')}}" required="">
              <option value="">Select Status</option>
              <option value="1">Active</option>
              <option value="0">Inactive</option>
            </select>
          </div>
          <div class="form-group">
            <label class="col-form-label">Description</label>
            <textarea class="form-control" rows="10" name="description" placeholder="product description" >{{$product->description}}</textarea>
        </div>
        
        <button type="submit" class="btn btn-primary"> <i class="fas fa-edit"></i> Update </button>
    </div>


    </div>
    
  </div>

<div class="col-md-4" style="padding:10px;padding-right:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Product Features</h3>
      </div>
      <!-- /.card-header -->
     
      <div style="padding:10px" >
        <select class="select2bs4" multiple="multiple" name="categories[]" data-placeholder="Select Categories" style="width: 100%;">
          @php
          $ids = $product->categories->pluck('id')->toArray();
          @endphp
          @foreach($categories as $category)
            <option value="{{$category->id}}" @if(in_array($category->id , $ids)) selected @endif > {{$category->name}} </option>
          @endforeach
        </select> 
      </div>
      <hr>

      <div class="p-2">
      <h2 >Featured Image</h2>
      
      <div class="input-group col s10 text-center "  >
        <span class="input-group-btn">
          <a id="lfm" data-input="thumbnail" data-preview="thumbnail" class="btn btn-primary text-white">
            <i class="fas fa-images"></i> Choose
          </a>
          <button class="btn btn-danger" type="button" onclick="remove_featured()"><i class="fas fa-times"></i> Clear</button>
        </span>
        <input id="thumbnail" class="form-control" type="hidden" name="featured_image" @if(isset($product->featured_image)) value="{{$product->featured_image}}" @endif onchange="show_image()" >
      </div>
      <hr>
        <div class="row">
           <div class="col s10 text-center">
            <img src="@if(isset($product->featured_image)) {{asset($product->featured_image)}} @else {{asset('img/no-image-found.jpg')}} @endif"  id="featured_image" style="margin-bottom: 15px; margin-top: 15px; width: auto; height: auto; max-width: 250px; max-height: 250px;"  />
          </div>
     </div>

    </div>
    <div class="form-group p-2  ">
      <label class="sr-only" for="inlineFormInputGroupUsername">Username</label>
      <div class="row ">
        <div class="col-md-10 m-2">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">$ </div>
        </div>
        <input type="text" value="{{$product->price}}" class="form-control" id="inlineFormInputGroupUsername" placeholder="Price" name="price" required>
      </div>
    </div>
    <div class="col-md-10 m-2">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">%</div>
        </div>
        <input type="text" class="form-control" value="{{$product->discount}}" name="discount" id="inlineFormInputGroupUsername" placeholder="Discount">
      </div>
    </div>
    </div>
    </div>
  </div>

  </div>
</form>
</div>



@endsection

@section('scripts')

<script>

  $('#post_title').on('keyup', function(){
    let pretty_url = slugify($(this).val());
    $('#url').html(slugify(pretty_url));
    $('#slug').val(pretty_url);
  });
  
  </script>

<script src=" {{asset('/admin-plugins/select2/js/select2.full.min.js')}}"></script>
<script>

  $('.select2bs4').select2({
    theme: 'bootstrap4',
  });

</script>

<script>
  var route_prefix = "/filemanager";

 </script>

 <script>
  {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
</script>
<script>
  $('#lfm').filemanager('image', {prefix: route_prefix});
  // $('#lfm').filemanager('file', {prefix: route_prefix});
</script>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "",
    selector: "textarea[name=description]",
    plugins: [
      "link image"
    ],
    relative_urls: false,
    height: 129,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + route_prefix + '?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>

<script>
  
  function show_image(){
    var thumb = $('#thumbnail').val();
    document.getElementById("featured_image").src = thumb;
    $('#featured_image').show();
  }

  function remove_featured(){
    //document.getElementById("featured_image").src =  '{{url('/')}}/img/no-image-found.jpg';
    document.getElementById("featured_image").src =  null;
    $('#featured_image').hide();
    $('#thumbnail').val(null);

  }

  $("#status").val("{{ $product->status }}");
  $("#subc_id").val("{{ $product->subc_id }}");
  $("#brand_id").val("{{ $product->brand_id }}");
  $("#product_on_sale").val("{{ $product->product_on_sale }}");
  
  
</script>



@endsection