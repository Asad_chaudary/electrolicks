@extends('layouts.admin')

@section('styles')
    <style>
      .wrapped{
      word-wrap: break-word;
      }
    </style>
@endsection
@section('content')


<div class="row">


    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Products</h3>
        <a href="{{route('admin.product.create')}}" class="btn btn-success float-right" > <i class="fas fa-plus"></i> Create Product</a>
      </div> 
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <thead>                  
            <tr>
              <th >#</th>
              <th>Name</th>
              <th> Image </th>
              <th>Description</th>
              <th>Price</th>
              
              <th> Discount </th>
              <th> On Sale </th>
              <th> Status </th>
              <th>Created</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($products as $k => $product)
                  
            <tr>
              <td>{{$k+1}}.</td>
              <td>{{$product->title}}</td>
              <td>
                <img src="{{ asset('/uploads/products/thumbnails'.'/'.$product->featured_image ) }}" width="100px" height="100px"> 
              </td>
              <td width="50%" style="max-width:195px; word-wrap:break-word;">{{ str_limit(strip_tags($product->description) , 200 )}} </td>
              <td> {{$product->price}} </td>
              
              <td> {{ $product->discount }} </td>
              <td>
                <a class="product-on-sale btn @if($product->status == 1) btn-success @else btn-primary @endif" href="javascript::void();" onclick="event.preventDefault();
                                     document.getElementById('product-on-sale-{{ $product->id }}').submit();">
                                  {{ $product->status == 1 ? 'Yes' : 'No'  }} 
                 </a>
                <form id="product-on-sale-{{ $product->id }}" action="{{ route('admin.product.on.sale',$product->id) }}" method="POST" style="display: none;">
                 @method('post')
                        @csrf
                </form> 
              </td>

              <td>
                <a class="status-products btn @if($product->status == 1) btn-success @else btn-primary @endif" href="javascript::void();" onclick="event.preventDefault();
                                     document.getElementById('status-products-{{ $product->id }}').submit();">
                                  {{ $product->status == 1 ? 'Active' : 'Inactive'  }} 
                 </a>
                <form id="status-products-{{ $product->id }}" action="{{ route('admin.product.status',$product->id) }}" method="POST" style="display: none;">
                 @method('post')
                        @csrf
                </form> 
              </td>
              <td> {{$product->created_at->toDateString() }} </td>
              <td> <a onclick="deleteProduct({{$product->id}})" href="javascript:;" class="text-danger"> <i class="fas fa-trash"></i> </a> 
               
              <a href="{{route('admin.product.edit' , $product->id)}}" class="text-primary"> <i class="fas fa-edit"></i> </a>

              <form id="delete-product-{{$product->id}}" action="{{route('admin.product.destroy' , $product->id)}}" method="POST" style="display: none" >
                @csrf 
                @method('DELETE')
              </form>

              </td>
            </tr>
            @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
  <script>
    function deleteProduct(id){

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          document.getElementById('delete-product-'+id).submit();
        }
      });

    }   
  </script>
@endsection