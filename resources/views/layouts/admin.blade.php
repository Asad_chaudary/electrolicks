
<html lang="en" style="height: auto;"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
  
    <title>ElectricStore</title>
  
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <link rel="stylesheet" href="{{asset('admin-dist/css/adminlte.min.css')}}">

<script src="{{asset('admin-dist/js/adminlte.js')}}" defer></script>


  <link rel="stylesheet" href="{{asset('admin-plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href=" {{asset('admin-plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('admin-plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('admin-plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('admin-plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('admin-plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('admin-plugins/summernote/summernote-bs4.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    
    <link rel="stylesheet" href="{{asset('css/app.css')}}" >
    <script src="{{asset('js/app.js')}} " ></script>

   <link id="noteanywherecss" media="screen" type="text/css" rel="stylesheet" href="data:text/css,.note-anywhere%20.closebutton%7Bbackground-image%3A%20url%28chrome-extension%3A//bohahkiiknkelflnjjlipnaeapefmjbh/asset/deleteButton.png%29%3B%7D%0A"> 


  <style>
    .navbar-badge {
      position: initial;
    }
    .dropdown-menu{
      min-width: 280px;
    }


  </style>

    @yield('styles')

</head>
  <body class="sidebar-mini fixed " style="height: auto;">
  <div class="wrapper">


  
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        {{--  <li class="nav-item d-none d-sm-inline-block">
          <a href="index3.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="#" class="nav-link">Contact</a>
        </li>  --}}
      </ul>
  
      <!-- SEARCH FORM -->
      {{--  <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
          <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>  --}}
  
      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-comments"></i>
            <span class="badge badge-danger navbar-badge">3</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="{{ asset('/') }}/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Brad Diesel
                    <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">Call me whenever you can...</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            
            <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>

          </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li>
        {{--  <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i class="fas fa-th-large"></i></a>
        </li>  --}}
      </ul>
    </nav>
    <!-- /.navbar -->
  
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="{{url('/admin')}}" class="brand-link">
        <img src="{{ asset('/') }}/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">ElectricStore</span>
      </a>
  
      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
             @if(isset(auth()->user()->avatar) )
            <img src="{{asset('/user_avatar/'.auth()->user()->avatar)}}" class="img-circle elevation-2" alt="User Image">
            @else
            <img src="{{asset('img/no-image-found.jpg')}}" class="img-circle elevation-2" alt="User Image">

            @endif 
            {{-- <img src="/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> --}}

          </div>
          <div class="info">
            <a href="{{route('admin.user.profile' , auth()->user()->id )}}" class="d-block">{{auth()->user()->name }}</a>
          </div>
        </div>
        
        {{--  @if (strpos(url()->current() , '/dashboard')) active }} @endif  --}}

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            <li class="nav-item">
              <a href="{{route('admin.index')}}" class="nav-link {{ request()->is('admin') ? 'active' : '' }}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                  Dashboard
                </p>
              </a>
            </li>
            <li class="nav-item has-treeview @if(Request::is('admin/categories') || Request::is('admin/categories/*') || Request::is('admin/subcategory-type') || Request::is('admin/subcategory-type/*') || Request::is('admin/subcategories') || Request::is('admin/subcategories/*') || Request::is('admin/product') || Request::is('admin/product/*') || Request::is('admin/brands') || Request::is('admin/brands/*') || Request::is('admin/featured-products') || Request::is('admin/featured-products/*') || Request::is('admin/new-products') || Request::is('admin/new-products/*') || Request::is('admin/hot-products') || Request::is('admin/hot-products/*')) menu-open @endif">
              <a href="/docs/3.0/components" class="nav-link @if(Request::is('admin/categories') || Request::is('admin/categories/*') || Request::is('admin/subcategory-type') || Request::is('admin/subcategory-type/*') || Request::is('admin/subcategories') || Request::is('admin/subcategories/*') || Request::is('admin/product') || Request::is('admin/product/*') || Request::is('admin/brands') || Request::is('admin/brands/*') || Request::is('admin/featured-products') || Request::is('admin/featured-products/*') || Request::is('admin/new-products') || Request::is('admin/new-products/*') || Request::is('admin/hot-products') || Request::is('admin/hot-products/*')) active @endif">
                <i class="nav-icon fas fa-th"></i>
                <p>
                  Main Menu
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
                <ul class="nav nav-treeview" style="display: @if(Request::is('admin/categories') || Request::is('admin/categories/*') || Request::is('admin/subcategory-type') || Request::is('admin/subcategory-type/*') || Request::is('admin/subcategories') || Request::is('admin/subcategories/*') || Request::is('admin/product') || Request::is('admin/product/*') || Request::is('admin/brands') || Request::is('admin/brands/*') || Request::is('admin/featured-products') || Request::is('admin/featured-products/*') || Request::is('admin/new-products') || Request::is('admin/new-products/*') || Request::is('admin/hot-products') || Request::is('admin/hot-products/*')) block @else none @endif;">
                  <li class="nav-item">
                    <a href="{{ route('categories.index') }}" class="nav-link @if(Request::is('admin/categories')) active @endif">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Category</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('subcategory-type.index') }}" class="nav-link @if(Request::is('admin/subcategory-type') || Request::is('admin/subcategory-type/*')) active @endif">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Subcategory Type</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('subcategories.index') }}" class="nav-link @if(Request::is('admin/subcategories') || Request::is('admin/subcategories/*')) active @endif">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Subcategory</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.brands.index') }}" class="nav-link @if(Request::is('admin/brands') || Request::is('admin/brands/*')) active @endif">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Brands</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.product.index') }}" class="nav-link @if(Request::is('admin/product') || Request::is('admin/product/*')) active @endif">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Products</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="{{ route('featured-products.index') }}" class="nav-link @if(Request::is('admin/featured-products') || Request::is('admin/featured-products/*')) active @endif">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Featured Products</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('new-products.index') }}" class="nav-link @if(Request::is('admin/new-products') || Request::is('admin/new-products/*')) active @endif">
                      <i class="far fa-circle nav-icon"></i>
                      <p>New Products</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('hot-products.index') }}" class="nav-link @if(Request::is('admin/hot-products') || Request::is('admin/hot-products/*')) active @endif">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Hot Products</p>
                    </a>
                  </li>
                </ul>
            </li>
            <li class="nav-item has-treeview @if(Request::is('admin/home-slider') || Request::is('admin/home-slider/*') || Request::is('admin/banner-images') || Request::is('admin/banner-images/*')) menu-open @endif">
              <a href="/docs/3.0/components" class="nav-link @if(Request::is('admin/home-slider') || Request::is('admin/home-slider/*') || Request::is('admin/banner-images') || Request::is('admin/banner-images/*')) active @endif">
                <i class="nav-icon fas fa-th"></i>
                <p>
                  General Settings
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
                <ul class="nav nav-treeview" style="display: @if(Request::is('admin/home-slider') || Request::is('admin/home-slider/*') || Request::is('admin/banner-images') || Request::is('admin/banner-images/*')) block @else none @endif;">
                  <li class="nav-item">
                    <a href="{{ route('home-slider.index') }}" class="nav-link @if(Request::is('admin/home-slider')) active @endif">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Home Slider</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('banner-images.index') }}" class="nav-link @if(Request::is('admin/banner-images') || Request::is('admin/banner-images/*')) active @endif">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Banner Images</p>
                    </a>
                  </li>
                 
                </ul>
            </li>
            
            <li class="nav-item">
              <a href="{{route('admin.users.index')}}" class="nav-link @if (strpos(url()->current() , '/users')) active }} @endif">
                <i class="nav-icon fas fa-users"></i>
                <p>
                  Users
                </p>
              </a>
            </li> 
            {{-- <li class="nav-item">
              <a href="{{route('admin.product.index')}}" class="nav-link @if (strpos(url()->current() , '/product')) active }} @endif">
                <i class="nav-icon fas fa-paste"></i>
                <p>
                  Products
                </p>
              </a>
            </li> --}}
            {{-- <li class="nav-item">
              <a href="{{route('admin.brands.index')}}" class="nav-link @if (strpos(url()->current() , '/brands')) active }} @endif">
                <i class="nav-icon fas fa-business-time"></i>
                <p>
                  Brands
                </p>
              </a>
            </li> --}}
            <li class="nav-item">
              <a href="{{route('admin.vendors.index')}}" class="nav-link @if (strpos(url()->current() , '/vendors')) active }} @endif">
                <i class="nav-icon fa fa-industry"></i>
                <p>
                  Vendors
                </p>
              </a>
            </li>
           
            <li class="nav-item">
              <a href="{{route('admin.category.index')}}" class="nav-link @if (strpos(url()->current() , '/category')) active }} @endif">
                <i class="nav-icon fas fa-list"></i>
                <p>
                  Categories
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.subcategory.index')}}" class="nav-link @if (strpos(url()->current() , '/subcategory')) active }} @endif">
                <i class="nav-icon fas fa-list-alt"></i>
                <p>
                  Sub Categories
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.delivery_boys.index')}}" class="nav-link @if (strpos(url()->current() , '/delivery_boys')) active }} @endif">
                <i class="nav-icon fas fa-truck-loading"></i>
                <p>
                  Delivery Boy
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.coupens.index')}}" class="nav-link @if (strpos(url()->current() , '/coupens')) active }} @endif">
                <i class="nav-icon fas fa-tags"></i>
                <p>
                  Coupens
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.media-managment')}}" class="nav-link @if (strpos(url()->current() , '/media')) active }} @endif">
                <i class="nav-icon fas fa-images"></i>
                <p>
                  Media Management
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.index')}}" class="nav-link {{ request()->is('') ? 'active' : '' }}">
                <i class="nav-icon fas fa-sticky-note"></i>
                <p>
                  Order Management
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.newsletters.index')}}" class="nav-link @if (strpos(url()->current() , '/newsletters')) active }} @endif">
                <i class="nav-icon fas fa-newspaper"></i>
                <p>
                  Newsletter
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link">
                <i class="nav-icon fas fa-power-off"></i>
                <p>
                  Logout
                </p>
              </a>
           </a>

           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
               @csrf
           </form>

            </li>

          </ul>
        </nav>
      </div>
      <!-- /.sidebar -->
    </aside>
  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: 211px;">

      <section class="content">
        <div class="container-fluid">

      @yield('content')

        </div>
      </section>
    </div>
    <!-- /.content-wrapper -->
  
    <!-- Control Sidebar -->
    {{--  <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>  --}}
    <!-- /.control-sidebar -->
  
    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        Anything you want
      </div>
      <!-- Default to the left -->
      <strong>Copyright © 2020 <a href="#">ElectricStore</a>.</strong> All rights reserved.
    </footer>
  <div id="sidebar-overlay"></div></div>
  <!-- ./wrapper -->
  
  <!-- REQUIRED SCRIPTS -->
  
  <!-- jQuery -->
  {{--  <script src="plugins/jquery/jquery.min.js"></script>  --}}
  <!-- Bootstrap 4 -->
  {{--  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>  --}}
  <!-- AdminLTE App -->
  {{--  <script src="dist/js/adminlte.min.js"></script>  --}}
  

  
<script src="{{asset('admin-plugins/jquery-ui/jquery-ui.min.js')}}"></script>


<script src="{{asset('admin-plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('admin-plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('admin-plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('admin-plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('admin-plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('admin-plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('admin-plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('admin-plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('admin-plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('admin-plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('admin-plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>



  @yield('scripts')
  
  </body>
  </html>
