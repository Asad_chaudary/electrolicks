<div class="header-bottom compact-hidden">
   <div class="container">
      <div class="row">
         <div class="sidebar-menu col-md-3 col-sm-6 col-xs-8 ">
            <div class="responsive so-megamenu ">
               <div class="so-vertical-menu no-gutter compact-hidden">
                  <nav class="navbar-default">
                     <div class="container-megamenu vertical {{ request()->is('/') ? 'open' : '' }}">
                        <div id="menuHeading">
                           <div class="megamenuToogle-wrapper">
                              <div class="megamenuToogle-pattern">
                                 <div class="container">
                                    <div> <span></span> <span></span> <span></span> </div>
                                    All Categories <i class="fa pull-right arrow-circle fa-chevron-circle-up"></i> 
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="navbar-header">
                           <button type="button" id="show-verticalmenu" data-toggle="collapse" class="navbar-toggle fa fa-list-alt"> </button> All Categories 
                        </div>
                        <div class="vertical-wrapper">
                           <span id="remove-verticalmenu" class="fa fa-times"></span>
                           <div class="megamenu-pattern">
                              <div class="container">
                                 <ul class="megamenu">
                                    @if(!empty(category()))
                                        @foreach(category() as $key => $value)
                                        <li class="item-vertical style1 with-sub-menu hover">
                                            <p class="close-menu"></p>
                                            <a href="#" class="clearfix"> 
                                                <img src="{{ asset('uploads/category/icons'.'/'.$value->c_icon) }}" alt="icon"> 
                                                <span> {{ $value->c_name }} </span>

                                                @if(isset($value->subcategory_type) && count($value->subcategory_type) > 0)
                                                <b class="caret"></b> 
                                                @endif
                                            </a>
                                          @if(isset($value->subcategory_type) && count($value->subcategory_type) > 0)
                                           <div class="sub-menu" data-subwidth="100">
                                              <div class="content">
                                                 <div class="row">
                                                    <div class="col-sm-12">
                                                       <div class="row">
                                                          <div class="col-md-4 static-menu">
                                                             <div class="menu">
                                                                <ul>
                                                                  @foreach($value->subcategory_type as $index => $subcat_type)
                                                                   <li>
                                                                      <a href="#" class="main-menu"> {{ $subcat_type['sct_name'] }} </a>
                                                                      @if(isset($subcat_type['subcategory']) && !empty($subcat_type['subcategory']))
                                                                        <ul>
                                                                          @foreach($subcat_type['subcategory'] as $i => $subcat)
                                                                           <li>
                                                                              <a href="#">
                                                                                {{ $subcat['sc_name'] }}
                                                                              </a>
                                                                            </li>
                                                                           @endforeach
                                                                           {{-- <li><a href="#">Accessories for i Pad</a></li>
                                                                           <li><a href="#">Accessories for iPhone</a></li>
                                                                           <li><a href="#">Bags, Holiday Supplies</a></li>
                                                                           <li><a href="#">Car Alarms and Security</a></li>
                                                                           <li><a href="#">Car Audio &amp; Speakers</a></li> --}}
                                                                        </ul>
                                                                      @endif
                                                                   </li>
                                                                   @endforeach
                                                                   {{-- <li>
                                                                      <a href="#" class="main-menu">Cables &amp; Connectors</a>
                                                                      <ul>
                                                                         <li><a href="#">Cameras &amp; Photo</a></li>
                                                                         <li><a href="#">Electronics</a></li>
                                                                         <li><a href="#">Outdoor &amp; Traveling</a></li>
                                                                      </ul>
                                                                   </li> --}}
                                                                </ul>
                                                             </div>
                                                          </div>
                                                          {{-- <div class="col-md-4 static-menu">
                                                             <div class="menu">
                                                                <ul>
                                                                   <li>
                                                                      <a href="#" class="main-menu">Camping &amp; Hiking</a>
                                                                      <ul>
                                                                         <li><a href="#">Earings</a></li>
                                                                         <li><a href="#">Shaving &amp; Hair Removal</a></li>
                                                                         <li><a href="#">Salon &amp; Spa Equipment</a></li>
                                                                      </ul>
                                                                   </li>
                                                                   <li>
                                                                      <a href="#" class="main-menu">Smartphone &amp; Tablets</a>
                                                                      <ul>
                                                                         <li><a href="#">Sports &amp; Outdoors</a></li>
                                                                         <li><a href="#">Bath &amp; Body</a></li>
                                                                         <li><a href="#">Gadgets &amp; Auto Parts</a></li>
                                                                      </ul>
                                                                   </li>
                                                                </ul>
                                                             </div>
                                                          </div>
                                                          <div class="col-md-4 static-menu">
                                                             <div class="menu">
                                                                <ul>
                                                                   <li>
                                                                      <a href="#" class="main-menu">Bags, Holiday Supplies</a>
                                                                      <ul>
                                                                         <li><a href="#" onclick="window.location = '18_46';">Battereries &amp; Chargers</a></li>
                                                                         <li><a href="#" onclick="window.location = '24_64';">Bath &amp; Body</a></li>
                                                                         <li><a href="#" onclick="window.location = '18_45';">Headphones, Headsets</a></li>
                                                                         <li><a href="#" onclick="window.location = '18_30';">Home Audio</a></li>
                                                                      </ul>
                                                                   </li>
                                                                </ul>
                                                             </div>
                                                          </div> --}}
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                           @endif
                                        </li>
                                     @endforeach
                                    @endif
                                    <li class="item-vertical">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/10.png" alt="icon"> <span>Electronic</span> </a>
                                    </li>
                                    <li class="item-vertical with-sub-menu hover">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <span class="label"></span> <img src="image/theme/icons/3.png" alt="icon"> <span>Sports &amp; Outdoors</span> <b class="caret"></b> </a>
                                       <div class="sub-menu" data-subwidth="60">
                                          <div class="content">
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="row">
                                                      <div class="col-md-12 static-menu">
                                                         <div class="menu">
                                                            <ul>
                                                               <li>
                                                                  <a href="#" onclick="window.location = '81';" class="main-menu">Mobile Accessories</a>
                                                                  <ul>
                                                                     <li><a href="#" onclick="window.location = '33_63';">Gadgets &amp; Auto Parts</a></li>
                                                                     <li><a href="#" onclick="window.location = '24_64';">Bath &amp; Body</a></li>
                                                                     <li><a href="#" onclick="window.location = '17';">Bags, Holiday Supplies</a></li>
                                                                  </ul>
                                                               </li>
                                                               <li>
                                                                  <a href="#" onclick="window.location = '18_46';" class="main-menu">Battereries &amp; Chargers</a>
                                                                  <ul>
                                                                     <li><a href="#" onclick="window.location = '25_28';">Outdoor &amp; Traveling</a></li>
                                                                     <li><a href="#" onclick="window.location = '80';">Flashlights &amp; Lamps</a></li>
                                                                     <li><a href="#" onclick="window.location = '24_66';">Fragrances</a></li>
                                                                  </ul>
                                                               </li>
                                                               <li>
                                                                  <a href="#" onclick="window.location = '25_31';" class="main-menu">Fishing</a>
                                                                  <ul>
                                                                     <li><a href="#" onclick="window.location = '57_73';">FPV System &amp; Parts</a></li>
                                                                     <li><a href="#" onclick="window.location = '18';">Electronics</a></li>
                                                                     <li><a href="#" onclick="window.location = '20_76';">Earings</a></li>
                                                                     <li><a href="#" onclick="window.location = '33_60';">More Car Accessories</a></li>
                                                                  </ul>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="row banner">
                                                      <a href="#"> <img src="image/demo/cms/menu_bg2.jpg" alt="banner1"> </a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li class="item-vertical with-sub-menu hover">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/2.png" alt="icon"> <span>Health &amp; Beauty</span> <b class="caret"></b> </a>
                                       <div class="sub-menu" data-subwidth="100">
                                          <div class="content">
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="row">
                                                      <div class="col-md-4 static-menu">
                                                         <div class="menu">
                                                            <ul>
                                                               <li>
                                                                  <a href="#" class="main-menu">Car Alarms and Security</a>
                                                                  <ul>
                                                                     <li><a href="#">Car Audio &amp; Speakers</a></li>
                                                                     <li><a href="#">Gadgets &amp; Auto Parts</a></li>
                                                                     <li><a href="#">Gadgets &amp; Auto Parts</a></li>
                                                                     <li><a href="#">Headphones, Headsets</a></li>
                                                                  </ul>
                                                               </li>
                                                               <li>
                                                                  <a href="24" onclick="window.location = '24';" class="main-menu">Health &amp; Beauty</a>
                                                                  <ul>
                                                                     <li> <a href="#">Home Audio</a> </li>
                                                                     <li> <a href="#">Helicopters &amp; Parts</a> </li>
                                                                     <li> <a href="#">Outdoor &amp; Traveling</a> </li>
                                                                     <li> <a href="#">Toys &amp; Hobbies</a> </li>
                                                                  </ul>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                      <div class="col-md-4 static-menu">
                                                         <div class="menu">
                                                            <ul>
                                                               <li>
                                                                  <a href="#" class="main-menu">Electronics</a>
                                                                  <ul>
                                                                     <li> <a href="#">Earings</a> </li>
                                                                     <li> <a href="#">Salon &amp; Spa Equipment</a> </li>
                                                                     <li> <a href="#">Shaving &amp; Hair Removal</a> </li>
                                                                     <li> <a href="#">Smartphone &amp; Tablets</a> </li>
                                                                  </ul>
                                                               </li>
                                                               <li>
                                                                  <a href="#" class="main-menu">Sports &amp; Outdoors</a>
                                                                  <ul>
                                                                     <li> <a href="#">Flashlights &amp; Lamps</a> </li>
                                                                     <li> <a href="#">Fragrances</a> </li>
                                                                     <li> <a href="#">Fishing</a> </li>
                                                                     <li> <a href="#">FPV System &amp; Parts</a> </li>
                                                                  </ul>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                      <div class="col-md-4 static-menu">
                                                         <div class="menu">
                                                            <ul>
                                                               <li>
                                                                  <a href="#" class="main-menu">More Car Accessories</a>
                                                                  <ul>
                                                                     <li> <a href="#">Lighter &amp; Cigar Supplies</a> </li>
                                                                     <li> <a href="#">Mp3 Players &amp; Accessories</a> </li>
                                                                     <li> <a href="#">Men Watches</a> </li>
                                                                     <li> <a href="#">Mobile Accessories</a> </li>
                                                                  </ul>
                                                               </li>
                                                               <li>
                                                                  <a href="#" class="main-menu">Gadgets &amp; Auto Parts</a>
                                                                  <ul>
                                                                     <li> <a href="#">Gift &amp; Lifestyle Gadgets</a> </li>
                                                                     <li> <a href="#">Gift for Man</a> </li>
                                                                     <li> <a href="#">Gift for Woman</a> </li>
                                                                     <li> <a href="#">Gift for Woman</a> </li>
                                                                  </ul>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li class="item-vertical css-menu with-sub-menu hover">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/2.png" alt="icon"> <span>Smartphone &amp; Tablets</span> <b class="caret"></b> </a>
                                       <div class="sub-menu" data-subwidth="30" style="width: 270px; display: none; right: 0px;">
                                          <div class="content" style="display: none;">
                                             <div class="row">
                                                <div class="col-sm-12">
                                                   <div class="row">
                                                      <div class="col-sm-12 hover-menu">
                                                         <div class="menu">
                                                            <ul>
                                                               <li> <a href="#" class="main-menu">Headphones, Headsets</a> </li>
                                                               <li> <a href="#" class="main-menu">Home Audio</a> </li>
                                                               <li> <a href="#" class="main-menu">Health &amp; Beauty</a> </li>
                                                               <li> <a href="#" class="main-menu">Helicopters &amp; Parts</a> </li>
                                                               <li> <a href="#" class="main-menu">Helicopters &amp; Parts</a> </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li class="item-vertical">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/11.png" alt="icon"> <span>Flashlights &amp; Lamps</span> </a>
                                    </li>
                                    <li class="item-vertical">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/4.png" alt="icon"> <span>Camera &amp; Photo</span> </a>
                                    </li>
                                    <li class="item-vertical">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/5.png" alt="icon"> <span>Smartphone &amp; Tablets</span> </a>
                                    </li>
                                    <li class="item-vertical" style="display: none;">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/7.png" alt="icon"> <span>Outdoor &amp; Traveling Supplies</span> </a>
                                    </li>
                                    <li class="item-vertical" style="display: none;">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/6.png" alt="icon"> <span>Health &amp; Beauty</span> </a>
                                    </li>
                                    <li class="item-vertical" style="display: none;">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/8.png" alt="icon"> <span>Toys &amp; Hobbies </span> </a>
                                    </li>
                                    <li class="item-vertical" style="display: none;">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/12.png" alt="icon"> <span>Jewelry &amp; Watches</span> </a>
                                    </li>
                                    <li class="item-vertical" style="display: none;">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/13.png" alt="icon"> <span>Bags, Holiday Supplies</span> </a>
                                    </li>
                                    <li class="item-vertical" style="display: none;">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix"> <img src="image/theme/icons/13.png" alt="icon"> <span>More Car Accessories</span> </a>
                                    </li>
                                    <li class="loadmore"> <i class="fa fa-plus-square-o"></i> <span class="more-view">More Categories</span> </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </nav>
               </div>
            </div>
         </div>
         <!-- Search -->
         <div class="header-bottom-right  col-md-9 col-sm-6 col-xs-4 ">
            <div id="sosearchpro" class="col-sm-7 search-pro">
               <form method="GET" action="index.html">
                  <div id="search0" class="search input-group">
                     <div class="select_category filter_type icon-select">
                        <select class="no-border custom-select" name="category_id">
                           <option value="0">All Categories &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                           @if(!empty(category()))
                           @foreach(category() as $key => $value)
                            <option value="{{ $value->id }}"> {{ $value->c_name }} </option>
                           @endforeach
                           @endif
                           {{-- <option value="77">Cables &amp; Connectors</option>
                           <option value="82">Cameras &amp; Photo</option>
                           <option value="80">Flashlights &amp; Lamps</option>
                           <option value="81">Mobile Accessories</option>
                           <option value="79">Video Games</option>
                           <option value="20">Jewelry &amp; Watches</option>
                           <option value="76">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Earings</option>
                           <option value="26">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wedding Rings</option>
                           <option value="27">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Men Watches</option> --}}
                        </select>
                     </div>
                     <input class="autosearch-input form-control" type="text" value="" size="50" autocomplete="off" placeholder="Search" name="search"> <span class="input-group-btn">
                     <button type="submit" class="button-search btn btn-primary" name="submit_search"><i class="fa fa-search"></i></button>
                     </span> 
                  </div>
                  <input type="hidden" name="route" value="product/search" /> 
               </form>
            </div>
         </div>
         <!-- //end Search -->
      </div>
   </div>
</div>