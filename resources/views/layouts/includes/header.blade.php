
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="header-top-left form-inline col-lg-6 col-md-5 col-sm-6 compact-hidden hidden-sm hidden-xs">
                            
                        </div>
                        <div class="header-top-right collapsed-block text-right col-lg-6 col-md-7 col-sm-12 col-xs-12 compact-hidden">
                            <h5 class="tabBlockTitle visible-xs">More<a class="expander " href="#TabBlock-1"><i class="fa fa-angle-down"></i></a></h5>
                            <div class="tabBlock" id="TabBlock-1">
                                <ul class="top-link list-inline">
                                    <li class="account btn-group" id="my_account">
                                        <a href="#" title="My Account" class="btn btn-xs dropdown-toggle" data-toggle="dropdown"> <span class="hidden-xs"> @
                                            @if(auth()->user() ) {{auth()->user()->name}}  @else  My Account @endif </span> <span class="fa fa-angle-down "></span></a>
                                        <ul class="dropdown-menu "> @if(auth()->user() )
                                            <li><a href="{{route('dashboard.index')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                                            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Logout</a></li>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> @csrf </form> @else
                                            <li><a href="register.html"><i class="fa fa-user"></i> Register</a></li>
                                            <li><a href="login.html"><i class="fa fa-pencil-square-o"></i> Login</a></li> @endif </ul>
                                    </li>
                                    <li class="wishlist"><a href="wishlist.html" id="wishlist-total" class="top-link-wishlist" title="Wish List (2)"><span class="hidden-xs">Wish List (2)</span></a></li>
                                    <li class="checkout"><a href="checkout.html" class="top-link-checkout" title="Checkout"><span class="hidden-xs">Checkout</span></a></li>
                                </ul>
                                <div class="form-group languages-block ">
                                    {{-- <form action="index.html" method="post" enctype="multipart/form-data" id="bt-language">
                                        <a class="btn btn-xs dropdown-toggle" data-toggle="dropdown"> <img src="image/demo/flags/gb.png" alt="English" title="English"> <span class="hidden-xs">English</span> <span class="fa fa-angle-down"></span> </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="#"><img class="image_flag" src="image/demo/flags/gb.png" alt="English" title="English" /> English </a>
                                            </li>
                                            <li>
                                                <a href="#"> <img class="image_flag" src="image/demo/flags/lb.png" alt="Arabic" title="Arabic" /> Arabic </a>
                                            </li>
                                        </ul>
                                    </form> --}}
                                </div>
                                <div class="form-group currencies-block">
                                    {{-- <form action="index.html" method="post" enctype="multipart/form-data" id="currency">
                                        <a class="btn btn-xs dropdown-toggle" data-toggle="dropdown"> <span class="icon icon-credit "></span> US Dollar <span class="fa fa-angle-down"></span> </a>
                                        <ul class="dropdown-menu btn-xs">
                                            <li> <a href="#">(€)&nbsp;Euro</a></li>
                                            <li> <a href="#">(£)&nbsp;Pounds    </a></li>
                                            <li> <a href="#">($)&nbsp;US Dollar </a></li>
                                        </ul>
                                    </form> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Header Top