   <!-- End Social widgets -->
    <!-- Include Libs & Plugins
============================================ -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="{{asset('main-js/jquery-2.2.4.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/owl-carousel/owl.carousel.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/themejs/libs.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/unveil/jquery.unveil.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/countdown/jquery.countdown.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/datetimepicker/moment.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/jquery-ui/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/modernizr/modernizr-2.6.2.min.js')}}"></script>
    <!-- Theme files
============================================ -->
    <script type="text/javascript" src="{{asset('main-js/themejs/application.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/themejs/homepage.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/themejs/so_megamenu.js')}}"></script>
    <script type="text/javascript" src="{{asset('main-js/themejs/addtocart.js')}}"></script>


     <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var _token = "{{ csrf_token() }}";
        // alert(_token)
    </script>

    @stack('js')

</body>

</html>