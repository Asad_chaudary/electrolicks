@extends('layouts.admin')


@section('content')


<div class="row">

    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Update Delivery Boy</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form class="form-horizontal" action="{{route('admin.delivery_boys.update' , $boy->id)}}" method="POST">
        @method('PATCH')
          @csrf
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Name" name="name" value="{{$boy->name}}" required>
            </div>
          </div>
       <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail2" class="row float-right col-form-label">Email:</label>
          </div>
            <div class="col-sm-8">
              <input type="email" class="form-control" id="inputEmail2" placeholder="Email" name="email" value="{{$boy->email}}">
            </div>
          </div>
       <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail3" class="row float-right col-form-label">Phone Number:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail3" name="phone" placeholder="Phone Number" value="{{$boy->phone}}">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail4" class="row float-right col-form-label">City:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail4" placeholder="City" name="city" value="{{$boy->city}}">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail5" class="row float-right col-form-label">Address:</label>
            </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail5" placeholder="Address" name="address" value="{{$boy->address}}">
            </div>
          </div>
      
        <div>
            <button class="btn btn-success float-right">Update</button>
        </div>
      </form>

      
    </div>
    </div>
  
  </div>
</div>

@endsection