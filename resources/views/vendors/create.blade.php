@extends('layouts.admin')


@section('content')


<div class="row">

    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Create Vendor</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('admin.vendors.store')}}" method="POST">
          @csrf
         
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Name" name="name" value="{{old('name')}}" required>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail2" class="row float-right col-form-label ">Phone:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail2" placeholder="Phone Number" name="phone" value="{{old('phone')}}" >
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail3" class="row float-right col-form-label ">Address:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail3" placeholder="Address" name="address" value="{{old('address')}}" >
            </div>
          </div>


          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail4" class="row float-right col-form-label ">Description:</label>
          </div>
            <div class="col-sm-8">
              <textarea class="form-control" id="inputEmail4" rows="10" name="description" placeholder="Some description about vendors(optional)">{{old('description')}}</textarea>
            </div>
          </div>
      
        <div>
            <button class="btn btn-primary float-right">Create</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection