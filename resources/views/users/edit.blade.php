@extends('layouts.admin')


@section('content')


<div class="row">

    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Update User</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('admin.users.update' , $user->id)}}" method="POST">
          @method('PUT')
          @csrf

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Name" name="name" value="{{old('name')}}" required>
            </div>
          </div>

           <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail2" class="row float-right col-form-label ">Email:</label>
          </div>
            <div class="col-sm-8">
              <input type="email" id="inputEmail2" placeholder="Email" class="form-control" name="email" value="{{$user->email}}" required/>
            </div>
          </div>

        
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail3" class="row float-right col-form-label ">Password:</label>
          </div>
            <div class="col-sm-8">
              <input type="password" id="inputEmail3" placeholder="Password" class="form-control" name="password" value="" />
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail4" class="row float-right col-form-label ">Confirm Password:</label>
          </div>
            <div class="col-sm-8">
              <input type="password" id="inputEmail4" placeholder="Confirm Password" class="form-control" name="conf_password" value="" />
            </div>
          </div>

          {{-- <div class="form-group row" style="padding-left:20px; padding-right:20px"> --}}
          <div class="form-group row" style="padding-left:2%; padding-right:2%">
            <div class="col-sm-2">
            <label for="inputEmail4" class="row float-right col-form-label mr-1">Avatar:</label>
          </div>
            <div class="col-sm-8">
              <input type="file" class="custom-file-input" id="customFile" name="avatar">
              <label class="custom-file-label" for="customFile">Choose file</label>
              <div> {{$user->avatar}} </div>
            </div>
          </div>
        <div>
            <button class="btn btn-primary">Update</button>
        </div>

      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection