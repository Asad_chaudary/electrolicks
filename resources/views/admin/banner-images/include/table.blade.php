<div class="row">


    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Banner Images</h3>
        <a href="{{route('banner-images.create')}}" class="btn btn-success float-right" > <i class="fas fa-list"></i> Create Banner Images</a>
      </div> 
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <thead>                  
            <tr>
              <th>#</th>
              <th>Home page Top image</th>
              <th>Bootom 1</th>
              <th>Bootom 2</th>
              <th>Right 1</th>
              <th>Right 2</th>
              <th>Right 3</th>
              <th> Center </th>
              <th>Status</th>
              <th>Created</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($banner_images as $k => $value)
                  
            <tr>
              <td>{{$k+1}}.</td>
              <td>
                <img src="{{ asset('/uploads/banner_images/thumbnails'.'/'.$value->ht_banner_image ) }}" width="100px" height="100px"> 
                
              </td>
              <td>
                <img src="{{ asset('/uploads/banner_images/thumbnails'.'/'.$value->hb_banner_image_1 ) }}" width="100px" height="100px"> 
              </td>
              <td> 
                <img src="{{ asset('/uploads/banner_images/thumbnails'.'/'.$value->hb_banner_image_2 ) }}" width="100px" height="100px">
              </td>
              <td> 
                <img src="{{ asset('/uploads/banner_images/thumbnails'.'/'.$value->hr_banner_image_1 ) }}" width="100px" height="100px">
              </td>
              <td> 
                <img src="{{ asset('/uploads/banner_images/thumbnails'.'/'.$value->hr_banner_image_2 ) }}" width="100px" height="100px">
              </td>

              <td> 
                <img src="{{ asset('/uploads/banner_images/thumbnails'.'/'.$value->hr_banner_image_3 ) }}" width="100px" height="100px">
              </td>
              <td> 
                <img src="{{ asset('/uploads/banner_images/thumbnails'.'/'.$value->center_banner_image ) }}" width="100px" height="100px">
              </td>
              
              <td>
                <a class="status-banner-images btn @if($value->status == 1) btn-success @else btn-primary @endif" href="javascript::void();" onclick="event.preventDefault();
                                     document.getElementById('status-banner-images-{{ $value->id }}').submit();">
                                  {{ $value->status == 1 ? 'Active' : 'Inactive'  }} 
                 </a>
                <form id="status-banner-images-{{ $value->id }}" action="{{ route('banner.images.status',$value->id) }}" method="POST" style="display: none;">
                 @method('post')
                        @csrf
                </form> 
              </td>
              <td> {{$value->created_at->toDateString() }} </td>

              <td> <a onclick="deleteCat({{$value->id}})" href="javascript:;" class="text-danger"> <i class="fas fa-trash"></i> </a> 
               
              <a href="{{route('banner-images.edit' , $value->id)}}" class="text-primary"> <i class="fas fa-edit"></i> </a>

              <form id="delete-banner-images-{{$value->id}}" action="{{route('banner-images.destroy' , $value->id)}}" method="POST" style="display: none" >
                @csrf 
                @method('DELETE')
              </form>

              </td>
            </tr>
            @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
  
  </div>
</div>