<div class="row">

    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Update Home Slider</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('home-slider.update' , $home_slider->id )}}" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          
    
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="name" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{$home_slider->name}}" required>
            </div>
          </div>
        
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="image_name" class="row float-right col-form-label ">Image:</label>
          </div>
            <div class="col-sm-8">
              <input type="file" class="form-control" id="image_name " placeholder="" name="image_name" value="{{old('image_name')}}">
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-2">
              <label for="status" class="row float-right col-form-label ">Status:</label>
            </div>
            <div class="col-sm-8">
              <select class="custom-select" name="status" id="status" value="{{old('status')}}" required="">
                <option value="">Select Status</option>
                <option value="1">Active</option>
                <option value="0">Inactive</option>
              </select>
           
            </div>
          </div>
      
        <div>
            <button class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@section('scripts')
  <script type="text/javascript">
    $("#status").val("{{ $home_slider->status }}");
    $("#sct_id").val("{{ $home_slider->sct_id }}");
  </script>
@endsection