<div class="row">


    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Home Slider</h3>
        <a href="{{route('home-slider.create')}}" class="btn btn-success float-right" > <i class="fas fa-list"></i> Create Home Slider</a>
      </div> 
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <thead>                  
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Image</th>
              <th>Status</th>
              <th>Created</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($home_slider as $k => $value)
                  
            <tr>
              <td>{{$k+1}}.</td>
              <td>{{ $value->name }}</td>
              <td> 
                <img src="{{ asset('/uploads/home_slider/thumbnails'.'/'.$value->image_name ) }}" width="100px" height="100px">
               
              </td>
              <td>
                <a class="status-home-slider btn @if($value->status == 1) btn-success @else btn-primary @endif" href="javascript::void();" onclick="event.preventDefault();
                                     document.getElementById('status-home-slider-{{ $value->id }}').submit();">
                                  {{ $value->status == 1 ? 'Active' : 'Inactive'  }} 
                 </a>
                <form id="status-home-slider-{{ $value->id }}" action="{{ route('home.slider.status',$value->id) }}" method="POST" style="display: none;">
                 @method('post')
                        @csrf
                </form> 
              </td>
              <td> {{$value->created_at->toDateString() }} </td>

              <td> <a onclick="deleteCat({{$value->id}})" href="javascript:;" class="text-danger"> <i class="fas fa-trash"></i> </a> 
               
              <a href="{{route('home-slider.edit' , $value->id)}}" class="text-primary"> <i class="fas fa-edit"></i> </a>

              <form id="delete-home-slider-{{$value->id}}" action="{{route('home-slider.destroy' , $value->id)}}" method="POST" style="display: none" >
                @csrf 
                @method('DELETE')
              </form>

              </td>
            </tr>
            @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
  
  </div>
</div>