<div class="row">


    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Categories</h3>
        <a href="{{route('create.new.subcategory.type',$id)}}" class="btn btn-success float-right" > <i class="fas fa-list"></i> Create Category</a>
      </div> 
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <thead>                  
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>category_name</th>
              <th>Image</th>
              <th>Status</th>
              <th>Action</th>
            </tr> 
          </thead>
          <tbody>
              @foreach ($subcate_type as $k => $value)
             {{--  @php 
                dd($value);
              @endphp --}}
              @if(!empty($value['subcategory_type']))
            <tr>
              <td>{{$k+1}}.</td>
              <td>{{ $value->subcategory_type['sct_name'] }}</td>
        
              <td> {{ $value->subcategory_type->category_name->c_name }} </td>
              <td> 
                <img src="{{ asset('/uploads/subcategory_type/thumbnails'.'/'.$value->subcategory_type['image_name'] ) }}" width="100px" height="100px">
               
              </td>
              <td>
                <a class="status-category btn @if($value->status == 1) btn-success @else btn-primary @endif" href="javascript::void();" onclick="event.preventDefault();
                                     document.getElementById('status-category-{{ $value->subcategory_type->id }}').submit();">
                                  {{ $value->status == 1 ? 'Active' : 'Inactive'  }} 
                 </a>
                <form id="status-category-{{ $value->subcategory_type->id }}" action="{{ route('subcategory.typ.status',$value->subcategory_type->id) }}" method="POST" style="display: none;">
                 @method('post')
                        @csrf
                </form> 
              </td>
          
              <td>
              <a href="{{ route('new.subcategory',$value->subcategory_type->id ) }}" class="btn btn-primary"> 
                Subcategory
              </a> 

                <a onclick="deleteCat({{$value->id}})" href="javascript:;" class="text-danger"> <i class="fas fa-trash"></i> 
                </a> 
               
              <a href="{{route('subcategories.edit' , $value->id)}}" class="text-primary"> <i class="fas fa-edit"></i> </a>

              <form id="delete-subcategory-type-{{$value->id}}" action="{{route('destroy.new.subcategory.type' , $value->subcategory_type->id)}}" method="POST" style="display: none" >
                @csrf 
                @method('DELETE')
              </form>

              </td>
            </tr>
            @endif
            @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
  
  </div>
</div>