@extends('layouts.admin')


@section('content')


<div class="row">


    @if($errors->any() )
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger"> {{$error}} </p>
    @endforeach
    @endif

    @if(session()->has('success'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: '{{session("success")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('error'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{session("error")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif
    @if(session()->has('message'))
    <script>
        Swal.fire({
            position: 'center',
            icon: 'info',
            title: '{{session("message")}} ',
            showConfirmButton: false,
            timer: 1500
          });
    </script>
    @endif

<div class="col-md-12" style="padding:30px" >
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">SubCategories</h3>
        <a href="{{route('admin.subcategory.create')}}" class="btn btn-success float-right" > <i class="fas fa-list"></i> Create SubCategory</a>
      </div> 
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover">
          <thead>                  
            <tr>
              <th >#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Parent</th>
              <th>Created</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($subcategories as $k => $category)
                  
            <tr>
              <td>{{$k+1}}.</td>
              <td>{{$category->name}}</td>
              <td width="50%">{{ str_limit($category->description , 200 )}} </td>              
              <td> {{$category->parent->name ?? '-'}} </td>
              <td> {{$category->created_at->toDateString() }} </td>
              
              <td> <a onclick="deleteCat({{$category->id}})" href="javascript:;" class="text-danger"> <i class="fas fa-trash"></i> </a> 
               
              <a href="{{route('admin.subcategory.edit' , $category->id )}}" class="text-primary"> <i class="fas fa-edit"></i> </a>

              <form id="delete-category-{{$category->id}}" action="{{route('admin.subcategory.destroy' , $category->id)}}" method="POST" style="display: none" >
                @csrf 
                @method('DELETE')
              </form>

              </td>
            </tr>
            @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
  <script>
    function deleteCat(id){

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          document.getElementById('delete-category-'+id).submit();
        }
      });

    }   
  </script>
@endsection