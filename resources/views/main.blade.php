@extends('layouts.main')


@section('content')
    


<!-- Block Spotlight1  -->
<section class="so-spotlight1">
    <div class="container">
        <div class="row">
            <div id="yt_header_right" class="col-lg-offset-3 col-lg-9 col-md-12">
                <div class="slider-container "> 
                    <div id="so-slideshow" >
                        <div class="module">
                            <div class="yt-content-slider yt-content-slider--arrow1"  data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="0" data-items_column0="1" data-items_column1="1" data-items_column2="1"  data-items_column3="1" data-items_column4="1" data-arrows="yes" data-pagination="no" data-lazyload="yes" data-loop="no" data-hoverpause="yes">
                                @if(isset($home_slider) && !empty($home_slider))
                                    @foreach($home_slider as $key => $value)
                                        <div class="yt-content-slide">
                                            <a href="#">
                                                <img src="{{ asset('uploads/home_slider'.'/'.$value->image_name) }}" alt="{{ $value->name }}" class="img-responsive"  style="max-height: 425px !important;">
                                            </a>
                                        </div>
                                    @endforeach
                                @else
                                <div class="yt-content-slide">
                                    <a href="#"><img src="image/demo/slider/v2-slide1.jpg" alt="slider1" class="img-responsive"></a>
                                </div>
                                <div class="yt-content-slide">
                                    <a href="#"><img src="image/demo/slider/v2-slide2.jpg" alt="slider2" class="img-responsive"></a>
                                </div>
                                <div class="yt-content-slide">
                                    <a href="#"><img src="image/demo/slider/v2-slide3.jpg" alt="slider3" class="img-responsive"></a>
                                </div>
                                @endif
                            </div>
                            <div class="loadeding"></div>
                        </div>
                    </div>

                    
                
                </div>
            </div>
        </div>
    </div>  
</section>
<!-- //Block Spotlight1  -->

<!-- Block Spotlight2  -->
<section class="so-spotlight2">
    <div class="container">
        <div class="row">
            <div class="news-letter col-md-3 col-sm-12 col-xs-12">
                <div class="newsletter">
                    <h2>NewsLetter</h2>
                    <p class="page-heading-sub hidden-md hidden-sm"> Please sign up to the Market mailing list to receive updates on new arrivals, special offers and other discount information </p>
                    <form action="#" method="post">
                        <div class="form-group required">
                            <div class="input-box">
                                <input type="email" name="txtemail" id="txtemail" value="" placeholder="" class="form-control input-lg"> </div>
                            <div class="subcribe">
                                <button type="submit" class="btn btn-default btn-lg" onclick="return subscribe();">Subscribe</button> <span>Subscribe</span> </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="banner-html  hidden-xs col-md-9 col-sm-12 col-xs-12">
                <div class="module">
                    <div class="modcontent clearfix">
                        <div class="m-banner">
                            <div class="m-banner-right">
                                <div class="m-banner2 banners">
                                    <div>
                                        <a href="#">
                                            <img src="{{ asset($data['ht_banner_image']) }}" alt="banner1">
                                        </a>
                                    </div>
                                </div>
                                
                                <div class="m-banner34">
                                    <div class="m-banner3 banners">
                                        <div>
                                            <a href="#">
                                                <img src="{{ asset($data['hb_banner_image_1']) }}" alt="banner1">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="m-banner4 banners ">
                                        <div>
                                            <a href="#">
                                                <img src="{{ asset($data['hb_banner_image_2']) }}" alt="banner1">
                                            </a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="m-banner1 banners hidden-xs">
                                <div>
                                    <a href="#">
                                        <img src="{{ asset($data['hr_banner_image_1']) }}" alt="banner1">
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //Block Spotlight2  -->

<!-- Main Container  -->
<div class="main-container container">
    
    <div class="row">
        <div id="content" class="col-md-9 col-sm-12 col-xs-12">
            
            <div class="module tab-slider titleLine">
        

                <h3 class="modtitle">Featured Product</h3>
                <div id="so_listing_tabs_1" class="so-listing-tabs first-load module">
                    <div class="loadeding"></div>
                    <div class="ltabs-wrap">
                    <div class="ltabs-tabs-container"  data-delay="300" data-duration="600" data-effect="starwars" data-ajaxurl="" data-type_source="0" data-lg="3" data-md="2" data-sm="2" data-xs="1"  data-margin="30">
                        <!--Begin Tabs-->
                        <div class="ltabs-tabs-wrap"> 
                       
                            <div class="item-sub-cat">
                                <ul class="ltabs-tabs cf">
                                    @if(isset($subcategory) && !empty($subcategory))
                                    @foreach($subcategory as $key => $value)
                                        <li class="ltabs-tab @if($key == 0) tab-sel @endif" data-category-id="{{ $value->subcategory->id }}" data-active-content=".items-category-{{ $value->subcategory->id }}"> 
                                            <span class="ltabs-tab-label">
                                                {{ $value->subcategory->sc_name }}                      
                                            </span> 
                                        </li>
                                    @endforeach
                                    @endif
                                   
                                </ul>
                            </div>
                        </div>
                        <!-- End Tabs-->
                    </div>
                    <div class="ltabs-items-container">
                        <!--Begin Items-->
                        @if(isset($subcategory) && !empty($subcategory))
                                    @foreach($subcategory as $key => $value)
                        <div class="ltabs-items @if($key == 0) ltabs-items-selected @endif items-category-{{ $value->subcategory->id }} grid" data-total="10">
                            <div class="ltabs-items-inner ltabs-slider ">
                               
                                @php 
                                    $featured_products = featured_products($value->subcategory->id);
                                @endphp
                                @if(isset($featured_products) && !empty($featured_products))
                                @foreach($featured_products as $key => $product)
                                    <div class="ltabs-item product-layout">
                                        <div class="product-item-container">
                                            <div class="left-block">
                                                <div class="product-image-container second_img ">
                                                    <img src="image/demo/shop/resize/J9-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
                                                    <img src="image/demo/shop/resize/J5-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
                                                </div>
                                                
                                                <span class="label label-sale">-15%</span>
                                                
                                                <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
                                               
                                            </div>

                                         
                                        </div>
                                    </div>
                                @endforeach
                                @endif
                               
                            </div>
                            
                        </div>
                        @endforeach
                        @endif
                        <div class="ltabs-items items-category-18 grid" data-total="11">
                            <div class="ltabs-loading"></div>
                            
                        </div>
                        <div class="ltabs-items  items-category-25 grid" data-total="11">
                            <div class="ltabs-loading"></div>
                        </div>
                    </div>
                    <!--End Items-->
                    
                    </div>
                    
                </div>
            </div>
            <div class="module">
                <div class="modcontent clearfix">
                    <div class="banners">
                        <div>
                            <a href="#"><img src="{{ asset($data['center_banner_image']) }}" alt="left-image"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="module tab-slider titleLine">
            <h3 class="modtitle">New Products</h3>
            <div id="so_listing_tabs_2" class="so-listing-tabs first-load module">
                <div class="ltabs-wrap">
                    <div class="ltabs-tabs-container"  data-delay="300" data-duration="600" data-effect="starwars" data-ajaxurl="" data-type_source="0" data-lg="3" data-md="2" data-sm="2" data-xs="1"  data-margin="30">
                        <!--Begin Tabs-->
                        <div class="ltabs-tabs-wrap"> 
                        <span class="ltabs-tab-selected">Jewelry &amp; Watches	</span> <span class="ltabs-tab-arrow">▼</span>
                            <div class="item-sub-cat">
                                <ul class="ltabs-tabs cf">
                                    <li class="ltabs-tab tab-sel" data-category-id="20" data-active-content=".items-category-20"> <span class="ltabs-tab-label">Jewelry &amp; Watches						</span> </li>
                                    <li class="ltabs-tab " data-category-id="18" data-active-content=".items-category-18"> <span class="ltabs-tab-label">Electronics		</span> </li>
                                    <li class="ltabs-tab " data-category-id="25" data-active-content=".items-category-25"> <span class="ltabs-tab-label">Sports &amp; Outdoors	</span> </li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Tabs-->
                    </div>
                    <div class="ltabs-items-container">
                        <!--Begin Items-->
                        <div class="ltabs-items ltabs-items-selected items-category-20 grid" data-total="10">
                            <div class="ltabs-items-inner ltabs-slider ">
                                
                                <div class="ltabs-item product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container second_img ">
                                                <img src="image/demo/shop/resize/w1-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
                                                <img src="image/demo/shop/resize/w10-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
                                            </div>
                                            
                                            <!--full quick view block-->
                                            <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
                                            <!--end full quick view block-->
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4><a href="product.html">Dail Lulpa</a></h4>		
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    </div>
                                                </div>
                                                                    
                                                <div class="price">
                                                    <span class="price-new">$97.00</span> 
                                                </div>
                                            </div>
                                            
                                              <div class="button-group">
                                                <button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
                                                <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
                                              </div>
                                        </div><!-- right block -->
                                    </div>
                                </div>
                                <div class="ltabs-item product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container second_img ">
                                                <img src="image/demo/shop/resize/B5-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
                                                <img src="image/demo/shop/resize/B10-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
                                            </div>
                                            
                                            <!--full quick view block-->
                                            <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
                                            <!--end full quick view block-->
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4><a href="product.html">Et Spare</a></h4>		
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    </div>
                                                </div>
                                                                    
                                                <div class="price">
                                                    <span class="price-new">$65.00</span> 
                                                </div>
                                            </div>
                                            
                                              <div class="button-group">
                                                <button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
                                                <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
                                              </div>
                                        </div><!-- right block -->
                                    </div>
                                </div>	
                                <div class="ltabs-item product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container second_img ">
                                                <img src="image/demo/shop/resize/J5-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
                                                <img src="image/demo/shop/resize/J9-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
                                            </div>
                                            <!--Sale Label-->
                                            <span class="label label-sale">-15%</span>
                                            <!--full quick view block-->
                                            <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
                                            <!--end full quick view block-->
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4><a href="product.html">Cupim Bris</a></h4>		
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    </div>
                                                </div>
                                                                    
                                                <div class="price">
                                                    <span class="price-new">$50.00</span> 
                                                    <span class="price-old">$62.00</span>		 
                                                </div>
                                            </div>
                                            
                                              <div class="button-group">
                                                <button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
                                                <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
                                              </div>
                                        </div><!-- right block -->
                                    </div>
                                </div>
                                <div class="ltabs-item product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container second_img ">
                                                <img src="image/demo/shop/resize/e11-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
                                                <img src="image/demo/shop/resize/E3-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
                                            </div>
                                            <!--Sale Label-->
                                            <span class="label label-sale">-15%</span>
                                            <!--full quick view block-->
                                            <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
                                            <!--end full quick view block-->
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4><a href="product.html">Apple Cinema 30"</a></h4>		
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    </div>
                                                </div>
                                                                    
                                                <div class="price">
                                                    <span class="price-new">$50.00</span> 
                                                    <span class="price-old">$62.00</span>		 
                                                </div>
                                            </div>
                                            
                                              <div class="button-group">
                                                <button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
                                                <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
                                              </div>
                                        </div><!-- right block -->
                                    </div>
                                </div>
                                <div class="ltabs-item product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container second_img ">
                                                <img src="image/demo/shop/resize/m1-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
                                                <img src="image/demo/shop/resize/m3-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
                                            </div>
                                            <!--New Label-->
                                            <span class="label label-new">New</span>
                                            <!--full quick view block-->
                                            <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
                                            <!--end full quick view block-->
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4><a href="product.html">Cisi Chicken	</a></h4>		
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    </div>
                                                </div>
                                                                    
                                                <div class="price">
                                                    <span class="price-new">$59.00</span> 
                                                </div>
                                            </div>
                                            
                                              <div class="button-group">
                                                <button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
                                                <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
                                              </div>
                                        </div><!-- right block -->
                                    </div>
                                </div>
                                <div class="ltabs-item product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container second_img ">
                                                <img src="image/demo/shop/resize/B10-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
                                                <img src="image/demo/shop/resize/B9-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
                                            </div>
                                            <!--Sale Label-->
                                            
                                            <!--full quick view block-->
                                            <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
                                            <!--end full quick view block-->
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4><a href="product.html">Bint Beef</a></h4>		
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    </div>
                                                </div>
                                                                    
                                                <div class="price">
                                                    <span class="price-new">$97.00</span> 
                                                </div>
                                            </div>
                                            
                                              <div class="button-group">
                                                <button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
                                                <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
                                              </div>
                                        </div><!-- right block -->
                                    </div>
                                </div>
                                <div class="ltabs-item product-layout">
                                    <div class="product-item-container">
                                        <div class="left-block">
                                            <div class="product-image-container second_img ">
                                                <img src="image/demo/shop/resize/J9-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
                                                <img src="image/demo/shop/resize/J5-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
                                            </div>
                                            <!--Sale Label-->
                                            <span class="label label-sale">-15%</span>
                                            <!--full quick view block-->
                                            <a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
                                            <!--end full quick view block-->
                                        </div>
                                        <div class="right-block">
                                            <div class="caption">
                                                <h4><a href="product.html">Cupim Bris</a></h4>		
                                                <div class="ratings">
                                                    <div class="rating-box">
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    </div>
                                                </div>
                                                                    
                                                <div class="price">
                                                    <span class="price-new">$50.00</span> 
                                                    <span class="price-old">$62.00</span>		 
                                                </div>
                                            </div>
                                            
                                              <div class="button-group">
                                                <button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
                                                <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
                                                <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
                                              </div>
                                        </div><!-- right block -->
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="ltabs-items items-category-18 grid" data-total="11">
                            <div class="ltabs-loading"></div>
                            
                        </div>
                        <div class="ltabs-items  items-category-25 grid" data-total="11">
                            <div class="ltabs-loading"></div>
                        </div>
                    </div>
                    <!--End Items-->
                    
                    
                </div>
                
            </div>
        </div>

            <div class="module no-margin titleLine ">
                <h3 class="modtitle">COLLECTIONS</h3>
                <div class="modcontent clearfix">
                    <div id="collections_block" class="clearfix  block">
                        <ul class="width6">
                            <li class="collect collection_0">
                                <div class="color_co"><a href="#">Furniture</a> </div>
                            </li>
                            <li class="collect collection_1">
                                <div class="color_co"><a href="#">Gift idea</a> </div>
                            </li>
                            <li class="collect collection_2">
                                <div class="color_co"><a href="#">Cool gadgets</a> </div>
                            </li>
                            <li class="collect collection_3">
                                <div class="color_co"><a href="#">Outdoor activities</a> </div>
                            </li>
                            <li class="collect collection_4">
                                <div class="color_co"><a href="#">Accessories for</a> </div>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
        <aside class="col-md-3 col-xs-12  content-aside right_column">
            <div class="module latest-product titleLine">
<h3 class="modtitle">On Sale Product</h3>
<div class="modcontent ">
    @if(isset($sales_products) && !empty($sales_products))
    @foreach($sales_products as $key => $value)
        <div class="product-latest-item">
        <div class="media">
           <div class="media-left">
              <a href="#">
                <img src="{{ asset('uploads/products/thumbnails'.'/'.$value->featured_image) }}" alt="Cisi Chicken" title="Cisi Chicken" class="img-responsive" style="width: 100px; height: 82px;"></a>
           </div>
           <div class="media-body">
              <div class="caption">
                 <h4><a href="#"> {{ $value->title }} </a></h4>
                
                 <div class="price">
                    <span class="price-new"> {{ $value->price }} </span>
                 </div>
                 <div class="ratings">
                    <div class="rating-box">
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                    </div>
                 </div>
              </div>
              
           </div>
        </div>
    </div>
    @endforeach
    @endif
    <div class="product-latest-item">
        <div class="media">
           <div class="media-left">
              <a href="#"><img src="image/demo/shop/product/m1.jpg" alt="Cisi Chicken" title="Cisi Chicken" class="img-responsive" style="width: 100px; height: 82px;"></a>
           </div>
           <div class="media-body">
              <div class="caption">
                 <h4><a href="#">Sunt Molup</a></h4>
                
                 <div class="price">
                    <span class="price-new">$100.00</span>
                 </div>
                 <div class="ratings">
                    <div class="rating-box">
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                    </div>
                 </div>
              </div>
              
           </div>
        </div>
    </div>
    <div class="product-latest-item">
        <div class="media">
           <div class="media-left">
              <a href="#"><img src="image/demo/shop/product/m2.jpg" alt="Cisi Chicken" title="Cisi Chicken" class="img-responsive" style="width: 100px; height: 82px;"></a>
           </div>
           <div class="media-body">
              <div class="caption">
                 <h4><a href="#">Et Spare</a></h4>
                
                 <div class="price">
                    <span class="price-new">$99.00</span>
                 </div>
                 <div class="ratings">
                    <div class="rating-box">
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                    </div>
                 </div>
              </div>
              
           </div>
        </div>
    </div>
    <div class="product-latest-item">
        <div class="media">
           <div class="media-left">
              <a href="#"><img src="image/demo/shop/product/18.jpg" alt="Cisi Chicken" title="Cisi Chicken" class="img-responsive" style="width: 100px; height: 82px;"></a>
           </div>
           <div class="media-body">
              <div class="caption">
                 <h4><a href="#">Cisi Chicken</a></h4>
                
                 <div class="price">
                    <span class="price-new">$59.00</span>
                 </div>
                 <div class="ratings">
                    <div class="rating-box">
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                    </div>
                 </div>
              </div>
              
           </div>
        </div>
    </div>
    <div class="product-latest-item transition">
        <div class="media">
           <div class="media-left">
              <a href="#"><img src="image/demo/shop/product/9.jpg" alt="Cisi Chicken" title="Cisi Chicken" class="img-responsive" style="width: 100px; height:82px;"></a>
           </div>
           <div class="media-body">
              <div class="caption">
                 <h4><a href="#">Kevin Labor</a></h4>
                 <div class="price">
                    <span class="price-new">$245.00</span>
                 </div>
                 <div class="ratings">
                    <div class="rating-box">
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                    </div>
                 </div>
              </div>
             
           </div>
        </div>
     </div>
    
    
</div>

</div>
            <div class="module titleLine">
                <h3 class="modtitle">Clients say</h3>
                <div class="modcontent">
                    <div class="block-clientsay block ">
                        
                        <div class="yt-content-slider slider-clients-say"  data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="0" data-items_column0="1" data-items_column1="1" data-items_column2="1"  data-items_column3="1" data-items_column4="1" data-arrows="yes" data-pagination="no" data-lazyload="yes" data-loop="no" data-hoverpause="yes">
                            <div class="yt-content-slide">
                                <div class="client-cont">Aliquam ut tellus dignissim, cursus erat ultricies tellus. Nulla tempus sollicitudin mauris cursus dictum. Etiam id diam diam.</div>
                                <div class="client-info"><img src="image/demo/cms/client-img1.jpg" alt="">
                                    <div class="inner">Aliquam Tellus
                                        <p>Vitae Ornare Mauris</p>
                                    </div>
                                </div>
                            </div>
                            <div class="yt-content-slide">
                                <div class="client-cont">Aliquam ut tellus dignissim, cursus erat ultricies tellus. Nulla tempus sollicitudin mauris cursus dictum. Etiam id diam diam.</div>
                                <div class="client-info"><img src="image/demo/cms/client-img1.jpg" alt="">
                                    <div class="inner">Aliquam Tellus
                                        <p>Vitae Ornare Mauris</p>
                                    </div>
                                </div>
                            </div>
                            <div class="yt-content-slide">
                                <div class="client-cont">Aliquam ut tellus dignissim, cursus erat ultricies tellus. Nulla tempus sollicitudin mauris cursus dictum. Etiam id diam diam.</div>
                                <div class="client-info"><img src="image/demo/cms/client-img1.jpg" alt="">
                                    <div class="inner">Aliquam Tellus
                                        <p>Vitae Ornare Mauris</p>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="module">
                <div class="modcontent clearfix">
                    <div class="banners">
                        <div>
                            <a href="#"><img src="{{ asset($data['hr_banner_image_2']) }}" alt="banner1"></a>
                        </div>
                    </div>
                
                </div>
            </div>
            <div class="module">
                <div class="modcontent clearfix">
                    <div class="banners">
                        <div>
                            <a href="#"><img src="{{ asset($data['hr_banner_image_3']) }}" alt="banner1"></a>
                        </div>
                    </div>
                
                </div>
            </div>
        </aside>

    </div>
</div>
<!-- //Main Container -->
<!-- Block Spotlight3  -->
<section class="so-spotlight3">
    <div class="container">
        <div class="row">
            <div id="so_categories_173761471880018" class="so-categories module titleLine preset01-4 preset02-3 preset03-3 preset04-1 preset05-1">
                <h3 class="modtitle">Hot Categories</h3>
                <div class="wrap-categories">
                    <div class="cat-wrap theme3">
                        <div class='col-md-12'>
                            <div class="carousel slide media-carousel" id="media">
                                <div class="carousel-inner">
                                    @if(isset($hot_categories->category) && !empty($hot_categories->category))
                                    @foreach($hot_categories->category as $key => $value)

                                    @endforeach
                                    @endif
                                    <div class="item  active">
                                        <div class="row">
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> </div>
                                            </div>
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> </div>
                                            </div>
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> </div>
                                            </div>
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="row">
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> </div>
                                            </div>
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> </div>
                                            </div>
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> </div>
                                            </div>
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="row">
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> </div>
                                            </div>
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> </div>
                                            </div>
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> </div>
                                            </div>
                                            <div class="col-md-3 content-box">
                                                <div class="image-cat">
                                                    <a href="#" title="Automotive" target="_blank"> <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> </a> <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <a data-slide="prev" href="#media" class="left carousel-control" style="    margin-top: 65px;">‹</a> 
                                <a data-slide="next" href="#media" class="right carousel-control" style="    margin-top: 65px;">›</a>
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //Block Spotlight3  -->












{{-- start sliders --}}

            <h3>3 Images & Slide 1 Image At A Time</h3>
            <div class="js-slider isystkSlider" data-shift="1" data-carousel="true" >
              <div class="view-layer">
                <ul class="parent">
                  <li class="child">
                    <p>
                        <a href="#" title="Automotive" target="_blank"> 
                            <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> 
                        </a> 
                        <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 
                    </p>
                  </li>
                  <li class="child">
                    <p>
                        <a href="#" title="Automotive" target="_blank"> 
                            <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> 
                        </a> 
                        <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 
                      {{-- <img src="https://source.unsplash.com/Z_6iR6b_GpI/400x300" width="180px"> --}}
                    </p>
                  </li>
                  <li class="child">
                    <p>
                        <a href="#" title="Automotive" target="_blank"> 
                            <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> 
                        </a> 
                        <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 
                      {{-- <img src="https://source.unsplash.com/TGDfpvSD8Lc/400x300" width="180px"> --}}
                    </p>
                  </li>
                  <li class="child">
                    <p>
                        <a href="#" title="Automotive" target="_blank"> 
                            <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> 
                        </a> 
                        <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 
                      {{-- <img src="https://source.unsplash.com/mcIRkKcAOJQ/400x300" width="180px"> --}}
                    </p>
                  </li>
                  <li class="child">
                    <p>
                        <a href="#" title="Automotive" target="_blank"> 
                            <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> 
                        </a> 
                        <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 
                      {{-- <img src="https://source.unsplash.com/jmnw_FqdR8k/400x300" width="180px"> --}}
                    </p>
                  </li>
                  <li class="child">
                    <p>
                        <a href="#" title="Automotive" target="_blank"> 
                            <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> 
                        </a> 
                        <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 
                      {{-- <img src="https://source.unsplash.com/L2kkaayv3YY/400x300" width="180px"> --}}
                    </p>
                  </li>
                  <li class="child">
                    <p>
                        <a href="#" title="Automotive" target="_blank"> 
                            <img src="image/demo/shop/category/bags-holiday-supplies-gifts.jpg" title="Automotive" alt="Automotive" style="height: 140px;"> 
                        </a> 
                        <a class="btn-viewmore hidden-xs" href="#" title="View more">View more</a> 
                      {{-- <img src="https://source.unsplash.com/mO0JS7xinPs/400x300" width="180px"> --}}
                    </p>
                  </li>
                  <li class="child">
                    <p>
                      <img src="https://source.unsplash.com/RfH8FR8jXv4/400x300" width="180px">
                    </p>
                  </li>
                  <li class="child">
                    <p>
                      <img src="https://source.unsplash.com/ymnMEMj9X_4/400x300" width="180px">
                    </p>
                  </li>
                  <li class="child">
                    <p>
                      <img src="https://source.unsplash.com/NdZtmw_jlMk/400x300" width="180px">
                    </p>
                  </li>
                  <li class="child">
                    <p>
                      <img src="https://source.unsplash.com/73eB_6KvRjs/400x300" width="180px">
                    </p>
                  </li>
                  <li class="child">
                    <p>
                      <img src="https://source.unsplash.com/tJxi9o4voWI/400x300" width="180px">
                    </p>
                  </li>
                </ul>
              </div>
              <div>
                <p class="next-btn"><a href="#"><img src="{{ asset('images') }}/btn-next.png" alt="次へ" ></a></p>
                <p class="prev-btn"><a href="#"><img src="{{ asset('images') }}/btn-prev.png" alt="前へ" ></a></p>
              </div>
            </div>



@endsection

@push('js')


@endpush