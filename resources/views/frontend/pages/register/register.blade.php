@include('frontend.includes.head') 
@include('frontend.includes.header1')
<div class="main-container container">
		<ul class="breadcrumb">
			<li><a href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
			<li><a href="{{url('/')}}">Home</a></li>
			<li><a href="{{url('/signup')}}">Register</a></li>
		</ul>
		
		<div class="row">
			<div id="content" class="col-sm-12">
				<h2 class="title">Register Account</h2>
				<p>If you already have an account with us, please login at the <a href="#">login page</a>.</p>
				<style>
				.form-horizontal .control-label {
					font-size: 14px;
				}
					.form-horizontal input {
						/* background: #f4f4f5; */
					    border: 1px solid #f4f4f5;
					    border-bottom: 2px solid #bcc9d1;
					    border-radius: 4px 4px 0 0;
					    padding:8px 12px ;
					    height: 48px;
					}

					.form-horizontal input:focus {
						box-shadow: none;
						/* border: none; */
					    border-bottom-color: #1d2124;
					    background: none;
					    border-left-color: #bcc9d1;
					    border-right-color: #bcc9d1;
					    border-top-color: #bcc9d1;
						/* background-color: rgba(0, 0, 0, 0.1); */
					}
				</style>
				<form action="" method="post" enctype="multipart/form-data" class="form-horizontal account-register clearfix">
					<fieldset id="account">
						<legend></legend>
						<div class="form-group required" style="display: none;">
							<label class="col-sm-2 control-label">Customer Group</label>
							<div class="col-sm-10">
								<div class="radio">
									<label>
										<input type="radio" name="customer_group_id" value="1" checked="checked"> Default
									</label>
								</div>
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-firstname">First Name</label>
							<div class="col-sm-10">
								<input type="text" name="firstname" value="" placeholder="First Name" id="input-firstname" class="form-control">
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-email">E-Mail</label>
							<div class="col-sm-10">
								<input type="email" name="email" value="" placeholder="E-Mail" id="input-email" class="form-control">
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-telephone">Phone</label>
							<div class="col-sm-10">
								<input type="tel" name="telephone" value="" placeholder="Phone" id="input-telephone" class="form-control">
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-password">Password</label>
							<div class="col-sm-10">
								<input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control">
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-city">City</label>
							<div class="col-sm-10">
								<input type="text" name="city" value="" placeholder="City" id="input-city" class="form-control">
							</div>
						</div>						
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-password">File</label>
							<div class="col-sm-10">
								<input type="file" name="file" value="" placeholder="File" id="input-password" class="form-control">
							</div>
						</div>
					</fieldset>
						<!-- <div class="form-group required">
							<label class="col-sm-2 control-label" for="input-country">Country</label>
							<div class="col-sm-10">
								<select name="country_id" id="input-country" class="form-control">
									<option value=""> --- Please Select --- </option>
									<option value="244">Aaland Islands</option>
									<option value="1">Afghanistan</option>
									<option value="2">Albania</option>
									<option value="3">Algeria</option>
									<option value="4">American Samoa</option>
									<option value="5">Andorra</option>
									<option value="6">Angola</option>
								</select>
							</div>
						</div> -->
					<div class="buttons">
						<div class="pull-right">I have read and agree to the <a href="#" class="agree"><b>Pricing Tables</b></a>
							<input class="box-checkbox" type="checkbox" name="agree" value="1"> &nbsp;
							<input type="submit" value="Continue" class="btn btn-primary">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

@include('frontend.includes.footer')
@include('frontend.includes.scripts')