<!-- Block Spotlight1  -->
	<section class="so-spotlight1">
		<div class="container">
			<div class="row">
				<div id="yt_header_right" class="col-lg-offset-3 col-lg-9 col-md-12">
					<div class="slider-container "> 
						<div id="so-slideshow" >
							<div class="module">
								<div class="yt-content-slider yt-content-slider--arrow1"  data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="0" data-items_column0="1" data-items_column1="1" data-items_column2="1"  data-items_column3="1" data-items_column4="1" data-arrows="yes" data-pagination="no" data-lazyload="yes" data-loop="no" data-hoverpause="yes">
									 @if(isset($home_slider) && count($home_slider) > 0)
									 	
	                                    @foreach($home_slider as $key => $value)
		                                    <div class="yt-content-slide">
												<a href="#">
													<img src="{{ asset('uploads/home_slider'.'/'.$value->image_name) }}" alt="{{ $value->name }}" class="img-responsive" style="max-height: 425px !important;">
												</a>
											</div>
	                                    @endforeach
	                                @else
	                                <div class="yt-content-slide">
	                                    <a href="#"><img src="{{ asset('frontend-assets') }}/image/demo/slider/e1.png" alt="slider1" class="img-responsive"></a>
	                                </div>
	                                <div class="yt-content-slide">
	                                    <a href="#"><img src="{{ asset('frontend-assets') }}/image/demo/slider/e21.png" alt="slider2" class="img-responsive"></a>
	                                </div>
	                                <div class="yt-content-slide">
	                                    <a href="#"><img src="{{ asset('frontend-assets') }}/image/demo/slider/e7.png" alt="slider1" class="img-responsive"></a>
	                                </div>
	                                <div class="yt-content-slide">
	                                    <a href="#"><img src="{{ asset('frontend-assets') }}/image/demo/slider/e5.png" alt="slider2" class="img-responsive"></a>
	                                </div>
	                                @endif

								</div>
								<div class="loadeding"></div>
							</div>
						</div>

						
					
					</div>
				</div>
			</div>
		</div>  
	</section>
	<!-- //Block Spotlight1  -->