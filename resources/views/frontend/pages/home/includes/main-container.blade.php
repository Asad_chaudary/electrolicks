{{-- Main Container  --> --}}
<style type="text/css">
	
<style>
.col-item
{
    border: 2px solid #2323A1;
    border-radius: 5px;
    background: #FFF;
}
.col-item .photo img
{
    margin: 0 auto;
    width: 100%;
}

.col-item .info
{
    padding: 10px;
    border-radius: 0 0 5px 5px;
    margin-top: 1px;
}
.col-item:hover .info {
    background-color: rgba(215, 215, 244, 0.5); 
}
.col-item .price
{
    /*width: 50%;*/
    float: left;
    margin-top: 5px;
}

.col-item .price h5
{
    line-height: 20px;
    margin: 0;
}

.price-text-color
{
    color: #00990E;
}

.col-item .info .rating
{
    color: #003399;
}

.col-item .rating
{
    /*width: 50%;*/
    float: left;
    font-size: 17px;
    text-align: right;
    line-height: 52px;
    margin-bottom: 10px;
    height: 52px;
}

.col-item .separator
{
    border-top: 1px solid #FFCCCC;
}

.clear-left
{
    clear: left;
}

.col-item .separator p
{
    line-height: 20px;
    margin-bottom: 0;
    margin-top: 10px;
    text-align: center;
}

.col-item .separator p i
{
    margin-right: 5px;
}
.col-item .btn-add
{
    width: 50%;
    float: left;
}

.col-item .btn-add
{
    border-right: 1px solid #CC9999;
}

.col-item .btn-details
{
    width: 50%;
    float: left;
    padding-left: 10px;
}
.controls
{
    margin-top: 20px;
}
[data-slide="prev"]
{
    margin-right: 10px;
}

/* slider styles */
	/* override position and transform in 3.3.x */
.carousel-inner .item.left.active {
  transform: translateX(-33%);
}
.carousel-inner .item.right.active {
  transform: translateX(33%);
}

.carousel-inner .item.next {
  transform: translateX(33%)
}
.carousel-inner .item.prev {
  transform: translateX(-33%)
}

.carousel-inner .item.right,
.carousel-inner .item.left { 
  transform: translateX(0);
}


.carousel-control.left,.carousel-control.right {background-image:none;}
</style>


</style>
	<div class="main-container container">
		
		<div class="row">
			<div id="content" class="col-md-9 col-sm-12 col-xs-12">
				<div class="module tab-slider titleLine">

					<h3 class="modtitle">Featured Product</h3>
					<div id="so_listing_tabs_1" class="so-listing-tabs first-load module">
						<div class="loadeding"></div>
						{{-- start featured products listing  --}}
							<div class="col-lg-12 col-md-12 col-sm-12" style="border-bottom: 1px solid;">
						        <ul class="nav nav-tabs" role="tablist" style="float: right;">
						        @if(isset($featured_products) && !empty($featured_products))
						        @php 
						        	$ubcategory_name = '';
						        @endphp
						        @foreach($featured_products as $key => $value)
						        @if($value['subcategory']['sc_name'] != $ubcategory_name)
						          <li role="presentation" class="@if($key == 0) active @endif">
						          	<a href="#home_{{ $value['subcategory']['id']  }}" aria-controls="home" role="tab" data-toggle="tab">
						          		 <span> {{ $value['subcategory']['sc_name'] }} </span>
						          	</a>
						          </li>
						          @endif
						          @php 
						          	$ubcategory_name = $value['subcategory']['sc_name'];
						          @endphp
						          @endforeach
						          @endif
						          
						        </ul>
						    </div>
					        
					        <!-- Tab panes -->
					        <div class="tab-content">
					        @if(isset($featured_products) && !empty($featured_products))
					        	@php $ubcategory_name = '';  @endphp
					   			@foreach($featured_products as $key => $value)
					          <div role="tabpanel" class="tab-pane @if($key == 0) active @endif" id="home_{{ $value['subcategory']['id'] }}">
					          		<div class="col-md-12">
										<div class="carousel slide" id="myCarousel">
										  <div class="carousel-inner">
										  	@if(isset($value['subcategory']['product']) && !empty($value['subcategory']['product']))
										  	@foreach($value['subcategory']['product'] as $key => $product)
										    <div class="item @if($key == 0) active @endif">
										     	<div class="col-md-4">
										      		<a href="#">
										      			<img src="http://placehold.it/500/bbbbbb/fff&amp;text=1" class="img-responsive">
										      		</a>
										      	</div>
										    </div>
										    @endforeach
										    @endif
										    <div class="item">
										      <div class="col-md-4"><a href="#"><img src="http://placehold.it/500/CCCCCC/fff&amp;text=2" class="img-responsive"></a></div>
										    </div>
										    <div class="item">
										      <div class="col-md-4"><a href="#"><img src="http://placehold.it/500/eeeeee/fff&amp;text=3" class="img-responsive"></a></div>
										    </div>
										    <div class="item">
										      <div class="col-md-4"><a href="#"><img src="http://placehold.it/500/f4f4f4/fff&amp;text=4" class="img-responsive"></a></div>
										    </div>
										    <div class="item">
										      <div class="col-md-4"><a href="#"><img src="http://placehold.it/500/fcfcfc/333&amp;text=5" class="img-responsive"></a></div>
										    </div>
										    <div class="item">
										      <div class="col-md-4"><a href="#"><img src="http://placehold.it/500/f477f4/fff&amp;text=6" class="img-responsive"></a></div>
										    </div>
										  </div>
										  <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
										  <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
										</div>
									</div>
					          </div>
					          @endforeach
					          @endif
					          <div role="tabpanel" class="tab-pane" id="profile">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
					          <div role="tabpanel" class="tab-pane" id="messages">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
					          <div role="tabpanel" class="tab-pane" id="settings">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passage..</div>
					          <div role="tabpanel" class="tab-pane" id="extra">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passage..</div>
					        </div>
					 	     	{{-- end featured products  --}}
					</div>
				</div>
				<div class="module">
					<div class="modcontent clearfix">
						<div class="banners">
							<div>
								<a href="#"><img src="image/demo/cms/v2-banner-home.jpg" alt="left-image"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="module tab-slider titleLine">
					<h3 class="modtitle">New Products</h3>
					<div id="so_listing_tabs_2" class="so-listing-tabs first-load module">
						<div class="ltabs-wrap">
							<div class="ltabs-tabs-container"  data-delay="300" data-duration="600" data-effect="starwars" data-ajaxurl="" data-type_source="0" data-lg="3" data-md="2" data-sm="2" data-xs="1"  data-margin="30">
								<!--Begin Tabs-->
								<div class="ltabs-tabs-wrap"> 
								<span class="ltabs-tab-selected">Jewelry &amp; Watches	</span> <span class="ltabs-tab-arrow">▼</span>
									<div class="item-sub-cat">
										<ul class="ltabs-tabs cf">
											<li class="ltabs-tab tab-sel" data-category-id="20" data-active-content=".items-category-20"> <span class="ltabs-tab-label">Jewelry &amp; Watches						</span> </li>
											<li class="ltabs-tab " data-category-id="18" data-active-content=".items-category-18"> <span class="ltabs-tab-label">Electronics		</span> </li>
											<li class="ltabs-tab " data-category-id="25" data-active-content=".items-category-25"> <span class="ltabs-tab-label">Sports &amp; Outdoors	</span> </li>
										</ul>
									</div>
								</div>
								<!-- End Tabs-->
							</div>
							<div class="ltabs-items-container">
								<!--Begin Items-->
								<div class="ltabs-items ltabs-items-selected items-category-20 grid" data-total="10">
									<div class="ltabs-items-inner ltabs-slider ">
										
										<div class="ltabs-item product-layout">
											<div class="product-item-container">
												<div class="left-block">
													<div class="product-image-container second_img ">
														<img src="image/demo/shop/resize/w1-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
														<img src="image/demo/shop/resize/w10-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
													</div>
													
													<!--full quick view block-->
													<a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
													<!--end full quick view block-->
												</div>
												<div class="right-block">
													<div class="caption">
														<h4><a href="product.html">Dail Lulpa</a></h4>		
														<div class="ratings">
															<div class="rating-box">
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
															</div>
														</div>
																			
														<div class="price">
															<span class="price-new">$97.00</span> 
														</div>
													</div>
													
													  <div class="button-group">
														<button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
														<button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
														<button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
													  </div>
												</div><!-- right block -->
											</div>
										</div>
										<div class="ltabs-item product-layout">
											<div class="product-item-container">
												<div class="left-block">
													<div class="product-image-container second_img ">
														<img src="image/demo/shop/resize/B5-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
														<img src="image/demo/shop/resize/B10-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
													</div>
													
													<!--full quick view block-->
													<a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
													<!--end full quick view block-->
												</div>
												<div class="right-block">
													<div class="caption">
														<h4><a href="product.html">Et Spare</a></h4>		
														<div class="ratings">
															<div class="rating-box">
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
															</div>
														</div>
																			
														<div class="price">
															<span class="price-new">$65.00</span> 
														</div>
													</div>
													
													  <div class="button-group">
														<button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
														<button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
														<button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
													  </div>
												</div><!-- right block -->
											</div>
										</div>	
										<div class="ltabs-item product-layout">
											<div class="product-item-container">
												<div class="left-block">
													<div class="product-image-container second_img ">
														<img src="image/demo/shop/resize/J5-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
														<img src="image/demo/shop/resize/J9-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
													</div>
													<!--Sale Label-->
													<span class="label label-sale">-15%</span>
													<!--full quick view block-->
													<a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
													<!--end full quick view block-->
												</div>
												<div class="right-block">
													<div class="caption">
														<h4><a href="product.html">Cupim Bris</a></h4>		
														<div class="ratings">
															<div class="rating-box">
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
															</div>
														</div>
																			
														<div class="price">
															<span class="price-new">$50.00</span> 
															<span class="price-old">$62.00</span>		 
														</div>
													</div>
													
													  <div class="button-group">
														<button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
														<button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
														<button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
													  </div>
												</div><!-- right block -->
											</div>
										</div>
										<div class="ltabs-item product-layout">
											<div class="product-item-container">
												<div class="left-block">
													<div class="product-image-container second_img ">
														<img src="image/demo/shop/resize/e11-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
														<img src="image/demo/shop/resize/E3-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
													</div>
													<!--Sale Label-->
													<span class="label label-sale">-15%</span>
													<!--full quick view block-->
													<a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
													<!--end full quick view block-->
												</div>
												<div class="right-block">
													<div class="caption">
														<h4><a href="product.html">Apple Cinema 30"</a></h4>		
														<div class="ratings">
															<div class="rating-box">
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
															</div>
														</div>
																			
														<div class="price">
															<span class="price-new">$50.00</span> 
															<span class="price-old">$62.00</span>		 
														</div>
													</div>
													
													  <div class="button-group">
														<button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
														<button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
														<button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
													  </div>
												</div><!-- right block -->
											</div>
										</div>
										<div class="ltabs-item product-layout">
											<div class="product-item-container">
												<div class="left-block">
													<div class="product-image-container second_img ">
														<img src="image/demo/shop/resize/m1-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
														<img src="image/demo/shop/resize/m3-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
													</div>
													<!--New Label-->
													<span class="label label-new">New</span>
													<!--full quick view block-->
													<a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
													<!--end full quick view block-->
												</div>
												<div class="right-block">
													<div class="caption">
														<h4><a href="product.html">Cisi Chicken	</a></h4>		
														<div class="ratings">
															<div class="rating-box">
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
															</div>
														</div>
																			
														<div class="price">
															<span class="price-new">$59.00</span> 
														</div>
													</div>
													
													  <div class="button-group">
														<button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
														<button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
														<button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
													  </div>
												</div><!-- right block -->
											</div>
										</div>
										<div class="ltabs-item product-layout">
											<div class="product-item-container">
												<div class="left-block">
													<div class="product-image-container second_img ">
														<img src="image/demo/shop/resize/B10-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
														<img src="image/demo/shop/resize/B9-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
													</div>
													<!--Sale Label-->
													
													<!--full quick view block-->
													<a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
													<!--end full quick view block-->
												</div>
												<div class="right-block">
													<div class="caption">
														<h4><a href="product.html">Bint Beef</a></h4>		
														<div class="ratings">
															<div class="rating-box">
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
															</div>
														</div>
																			
														<div class="price">
															<span class="price-new">$97.00</span> 
														</div>
													</div>
													
													  <div class="button-group">
														<button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
														<button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
														<button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
													  </div>
												</div><!-- right block -->
											</div>
										</div>
										<div class="ltabs-item product-layout">
											<div class="product-item-container">
												<div class="left-block">
													<div class="product-image-container second_img ">
														<img src="image/demo/shop/resize/J9-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img-responsive" />
														<img src="image/demo/shop/resize/J5-270x270.jpg"  alt="Apple Cinema 30&quot;" class="img_0 img-responsive" />
													</div>
													<!--Sale Label-->
													<span class="label label-sale">-15%</span>
													<!--full quick view block-->
													<a class="quickview iframe-link visible-lg" data-fancybox-type="iframe"  href="quickview.html">  Quickview</a>
													<!--end full quick view block-->
												</div>
												<div class="right-block">
													<div class="caption">
														<h4><a href="product.html">Cupim Bris</a></h4>		
														<div class="ratings">
															<div class="rating-box">
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
																<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
															</div>
														</div>
																			
														<div class="price">
															<span class="price-new">$50.00</span> 
															<span class="price-old">$62.00</span>		 
														</div>
													</div>
													
													  <div class="button-group">
														<button class="addToCart" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42', '1');"><i class="fa fa-shopping-cart"></i> <span class="">Add to Cart</span></button>
														<button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');"><i class="fa fa-heart"></i></button>
														<button class="compare" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');"><i class="fa fa-exchange"></i></button>
													  </div>
												</div><!-- right block -->
											</div>
										</div>
										
									</div>
									
								</div>
								<div class="ltabs-items items-category-18 grid" data-total="11">
									<div class="ltabs-loading"></div>
									
								</div>
								<div class="ltabs-items  items-category-25 grid" data-total="11">
									<div class="ltabs-loading"></div>
								</div>
							</div>
							<!--End Items-->
							
							
						</div>
						
					</div>
				</div>
				<div class="module no-margin titleLine ">
					<h3 class="modtitle">COLLECTIONS</h3>
					<div class="modcontent clearfix">
						<div id="collections_block" class="clearfix  block">
							<ul class="width6">
								<li class="collect collection_0">
									<div class="color_co"><a href="#">Furniture</a> </div>
								</li>
								<li class="collect collection_1">
									<div class="color_co"><a href="#">Gift idea</a> </div>
								</li>
								<li class="collect collection_2">
									<div class="color_co"><a href="#">Cool gadgets</a> </div>
								</li>
								<li class="collect collection_3">
									<div class="color_co"><a href="#">Outdoor activities</a> </div>
								</li>
								<li class="collect collection_4">
									<div class="color_co"><a href="#">Accessories for</a> </div>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
			<aside class="col-md-3 col-xs-12  content-aside right_column">
				<div class="module latest-product titleLine">
				   <h3 class="modtitle">On Sale Product</h3>
				   <div class="modcontent ">
						@if(isset($sales_products) && !empty($sales_products))
    @foreach($sales_products as $key => $value)
        <div class="product-latest-item">
        <div class="media">
           <div class="media-left">
              <a href="#">
                <img src="{{ asset('uploads/products/thumbnails'.'/'.$value->featured_image) }}" alt="Cisi Chicken" title="Cisi Chicken" class="img-responsive" style="width: 100px; height: 82px;"></a>
           </div>
           <div class="media-body">
              <div class="caption">
                 <h4><a href="#"> {{ $value->title }} </a></h4>
                
                 <div class="price">
                    <span class="price-new"> {{ $value->price }} </span>
                 </div>
                 <div class="ratings">
                    <div class="rating-box">
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                    </div>
                 </div>
              </div>
              
           </div>
        </div>
    </div>
    @endforeach
    @endif
						<div class="product-latest-item">
							<div class="media">
							   <div class="media-left">
								  <a href="#"><img src="image/demo/shop/product/m2.jpg" alt="Cisi Chicken" title="Cisi Chicken" class="img-responsive" style="width: 100px; height: 82px;"></a>
							   </div>
							   <div class="media-body">
								  <div class="caption">
									 <h4><a href="#">Et Spare</a></h4>
									
									 <div class="price">
										<span class="price-new">$99.00</span>
									 </div>
									 <div class="ratings">
										<div class="rating-box">
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
										</div>
									 </div>
								  </div>
								  
							   </div>
							</div>
						</div>
						<div class="product-latest-item">
							<div class="media">
							   <div class="media-left">
								  <a href="#"><img src="image/demo/shop/product/18.jpg" alt="Cisi Chicken" title="Cisi Chicken" class="img-responsive" style="width: 100px; height: 82px;"></a>
							   </div>
							   <div class="media-body">
								  <div class="caption">
									 <h4><a href="#">Cisi Chicken</a></h4>
									
									 <div class="price">
										<span class="price-new">$59.00</span>
									 </div>
									 <div class="ratings">
										<div class="rating-box">
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
										</div>
									 </div>
								  </div>
								  
							   </div>
							</div>
						</div>
						<div class="product-latest-item transition">
							<div class="media">
							   <div class="media-left">
								  <a href="#"><img src="image/demo/shop/product/9.jpg" alt="Cisi Chicken" title="Cisi Chicken" class="img-responsive" style="width: 100px; height:82px;"></a>
							   </div>	
							   <div class="media-body">
								  <div class="caption">
									 <h4><a href="#">Kevin Labor</a></h4>
									 <div class="price">
										<span class="price-new">$245.00</span>
									 </div>
									 <div class="ratings">
										<div class="rating-box">
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
										   <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
										</div>
									 </div>
								  </div>
								 
							   </div>
							</div>
						 </div>
				   </div>
				   
				</div>
				<div class="module titleLine">
					<h3 class="modtitle">Clients say</h3>
					<div class="modcontent">
						<div class="block-clientsay block ">
							
							<div class="yt-content-slider slider-clients-say"  data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="0" data-items_column0="1" data-items_column1="1" data-items_column2="1"  data-items_column3="1" data-items_column4="1" data-arrows="yes" data-pagination="no" data-lazyload="yes" data-loop="no" data-hoverpause="yes">
								<div class="yt-content-slide">
									<div class="client-cont">Aliquam ut tellus dignissim, cursus erat ultricies tellus. Nulla tempus sollicitudin mauris cursus dictum. Etiam id diam diam.</div>
									<div class="client-info"><img src="image/demo/cms/client-img1.jpg" alt="">
										<div class="inner">Aliquam Tellus
											<p>Vitae Ornare Mauris</p>
										</div>
									</div>
								</div>
								<div class="yt-content-slide">
									<div class="client-cont">Aliquam ut tellus dignissim, cursus erat ultricies tellus. Nulla tempus sollicitudin mauris cursus dictum. Etiam id diam diam.</div>
									<div class="client-info"><img src="image/demo/cms/client-img1.jpg" alt="">
										<div class="inner">Aliquam Tellus
											<p>Vitae Ornare Mauris</p>
										</div>
									</div>
								</div>
								<div class="yt-content-slide">
									<div class="client-cont">Aliquam ut tellus dignissim, cursus erat ultricies tellus. Nulla tempus sollicitudin mauris cursus dictum. Etiam id diam diam.</div>
									<div class="client-info"><img src="image/demo/cms/client-img1.jpg" alt="">
										<div class="inner">Aliquam Tellus
											<p>Vitae Ornare Mauris</p>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
				<div class="module">
					<div class="modcontent clearfix">
						<div class="banners">
							<div>
								<a href="#"><img src="image/demo/cms/v2-banner-right.jpg" alt="banner1"></a>
							</div>
						</div>
					
					</div>
				</div>
				<div class="module">
					<div class="modcontent clearfix">
						<div class="banners">
							<div>
								<a href="#"><img src="image/demo/cms/v3-banner-home-left.jpg" alt="banner1"></a>
							</div>
						</div>
					
					</div>
				</div>
			</aside>
		</div>
	</div>
	<!-- //Main Container -->

@push('js')
	<script type="text/javascript">
		$('#myCarousel').carousel({
		  interval: 10000
		})

		$('#myCarousel_1').carousel({
		  interval: 10000
		});

$('.carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  }
  else {
  	$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});
	</script>
@endpush