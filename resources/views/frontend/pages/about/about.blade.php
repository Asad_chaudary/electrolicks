@include('frontend.includes.head') 
@include('frontend.includes.header1')
<!-- Main Container  -->
	<div class="main-container container">
		<ul class="breadcrumb">
			<li><a href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
			<li><a href="{{url('/')}}">Home</a></li>
			<li><a href="{{url('/about')}}">About Us</a></li>
		</ul>
		
		<div class="row">
			<div id="content" class="col-sm-12">
			
				<div class="about-us about-demo-2">
					<div class="row">
						<div class="col-lg-12 col-md-12 about-us-center">
							<div class="about-image-slider">
								<div id="ytcs579c0714641b9213691469843220" class="yt-content-slider owl-theme yt-content-slider-style-1 arrow-default owl2-carousel owl2-theme owl2-loaded" data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="" data-item_lg="1" data-item_sm="1" data-item_xs="1" data-arrows="yes" data-pagination="no" data-lazyload="yes" data-loop="no" data-hoverpause="no">
									<div class="yt-content-slide yt-clearfix yt-content-wrap"> <img src="image/demo/about/about-1.jpg" alt="About Us"> </div>
									<div class="yt-content-slide yt-clearfix yt-content-wrap"> <img src="image/demo/about/about-2.jpg" alt="About Us"> </div>
									<div class="yt-content-slide yt-clearfix yt-content-wrap"> <img src="image/demo/about/about-3.jpg" alt="About Us"> </div>
								</div>
								
							</div>
							<div class="content-description">
								<h2 class="about-title">About Us</h2>
								<center>
				<hr width="10%" style="border: 2px solid #F4A137;">
				</center>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </p>
								<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 what-client-say">
							<div class="client-say-content">
								<h2 class="about-title line-hori"><span>What Clients Say</span></h2>
								<div class="client-say-slider">
									<div id="ytcs579bfc965e489183711469840534" class="yt-content-slider owl-theme yt-content-slider-style-1 arrow-default owl2-carousel owl2-theme owl2-loaded" data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="30" data-item_lg="1" data-item_sm="1" data-item_xs="1" data-arrows="no" data-pagination="yes" data-lazyload="yes" data-loop="no" data-hoverpause="no">
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<p class="des-member des-client">“Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.”</p>
												<h3 class="name-member">Jennifer lawrence</h3>
												<p class="job-member">Co Founder</p>
											</div>
										</div>
										
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<p class="des-member des-client">“Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.”</p>
												<h3 class="name-member">Jennifer lawrence</h3>
												<p class="job-member">Co Founder</p>
											</div>
										</div>
									
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<p class="des-member des-client">“Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.”</p>
												<h3 class="name-member">Jennifer lawrence</h3>
												<p class="job-member">Co Founder</p>
											</div>
										</div>
									
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<p class="des-member des-client">“Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.”</p>
												<h3 class="name-member">Jennifer lawrence</h3>
												<p class="job-member">Co Founder</p>
											</div>
										</div>
										
									</div>
									
								</div>
							</div>
						</div>
					</div><br>
					<div class="row">
						<div class="col-lg-12 col-md-12 our-team">
							<div class="our-team-content">
								<h2 class="about-title text-center"><span>Our Team</span></h2>
								<center>
				<hr width="10%" style="border: 2px solid #F4A137;">
				</center>
								<div class="our-team-slider">
									<div id="ytcs579bfc965e78d103041469840534" class="yt-content-slider owl-theme yt-content-slider-style-1 arrow-default owl2-carousel owl2-theme owl2-loaded" data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="30" data-item_lg="4" data-item_sm="3" data-item_xs="1" data-arrows="yes" data-pagination="no" data-lazyload="yes" data-loop="no" data-hoverpause="no">
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<div class="member">
													<div class="member-image"> <img src="image/demo/about/ourmember01.png" alt="Image Client"> </div>
													<div class="member-info">
														<h3 class="name-member">Jennifer lawrence</h3>
														<p class="job-member">Co Founder</p>
														<p class="des-member">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer condimentum</p>
														<ul class="social-member">
															<li class="social-icon rss"><a class="fa fa-rss" href="#" title="RSS"><span>RSS</span></a>
															</li>
															<li class="social-icon facebook"><a class="fa fa-facebook" href="#" title="Facebook"><span>Facebook</span></a>
															</li>
															<li class="social-icon twitter"><a class="fa fa-twitter" href="#" title="Twitter"><span>Twitter</span></a>
															</li>
															<li class="social-icon google"><a class="fa fa-google" href="#" title="Google"><span>Google</span></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									
									
									
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<div class="member">
													<div class="member-image"> <img src="image/demo/about/ourmember02.png" alt="Image Client"> </div>
													<div class="member-info">
														<h3 class="name-member">Jennifer lawrence</h3>
														<p class="job-member">Co Founder</p>
														<p class="des-member">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer condimentum</p>
														<ul class="social-member">
															<li class="social-icon rss"><a class="fa fa-rss" href="#" title="RSS"><span>RSS</span></a>
															</li>
															<li class="social-icon facebook"><a class="fa fa-facebook" href="#" title="Facebook"><span>Facebook</span></a>
															</li>
															<li class="social-icon twitter"><a class="fa fa-twitter" href="#" title="Twitter"><span>Twitter</span></a>
															</li>
															<li class="social-icon google"><a class="fa fa-google" href="#" title="Google"><span>Google</span></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<div class="member">
													<div class="member-image"> <img src="image/demo/about/ourmember03.png" alt="Image Client"> </div>
													<div class="member-info">
														<h3 class="name-member">Jennifer lawrence</h3>
														<p class="job-member">Co Founder</p>
														<p class="des-member">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer condimentum</p>
														<ul class="social-member">
															<li class="social-icon rss"><a class="fa fa-rss" href="#" title="RSS"><span>RSS</span></a>
															</li>
															<li class="social-icon facebook"><a class="fa fa-facebook" href="#" title="Facebook"><span>Facebook</span></a>
															</li>
															<li class="social-icon twitter"><a class="fa fa-twitter" href="#" title="Twitter"><span>Twitter</span></a>
															</li>
															<li class="social-icon google"><a class="fa fa-google" href="#" title="Google"><span>Google</span></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<div class="member">
													<div class="member-image"> <img src="image/demo/about/ourmember04.png" alt="Image Client"> </div>
													<div class="member-info">
														<h3 class="name-member">Jennifer lawrence</h3>
														<p class="job-member">Co Founder</p>
														<p class="des-member">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer condimentum</p>
														<ul class="social-member">
															<li class="social-icon rss"><a class="fa fa-rss" href="#" title="RSS"><span>RSS</span></a>
															</li>
															<li class="social-icon facebook"><a class="fa fa-facebook" href="#" title="Facebook"><span>Facebook</span></a>
															</li>
															<li class="social-icon twitter"><a class="fa fa-twitter" href="#" title="Twitter"><span>Twitter</span></a>
															</li>
															<li class="social-icon google"><a class="fa fa-google" href="#" title="Google"><span>Google</span></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<div class="member">
													<div class="member-image"> <img src="image/demo/about/ourmember01.png" alt="Image Client"> </div>
													<div class="member-info">
														<h3 class="name-member">Jennifer lawrence</h3>
														<p class="job-member">Co Founder</p>
														<p class="des-member">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer condimentum</p>
														<ul class="social-member">
															<li class="social-icon rss"><a class="fa fa-rss" href="#" title="RSS"><span>RSS</span></a>
															</li>
															<li class="social-icon facebook"><a class="fa fa-facebook" href="#" title="Facebook"><span>Facebook</span></a>
															</li>
															<li class="social-icon twitter"><a class="fa fa-twitter" href="#" title="Twitter"><span>Twitter</span></a>
															</li>
															<li class="social-icon google"><a class="fa fa-google" href="#" title="Google"><span>Google</span></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<div class="member">
													<div class="member-image"> <img src="image/demo/about/ourmember03.png" alt="Image Client"> </div>
													<div class="member-info">
														<h3 class="name-member">Jennifer lawrence</h3>
														<p class="job-member">Co Founder</p>
														<p class="des-member">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer condimentum</p>
														<ul class="social-member">
															<li class="social-icon rss"><a class="fa fa-rss" href="#" title="RSS"><span>RSS</span></a>
															</li>
															<li class="social-icon facebook"><a class="fa fa-facebook" href="#" title="Facebook"><span>Facebook</span></a>
															</li>
															<li class="social-icon twitter"><a class="fa fa-twitter" href="#" title="Twitter"><span>Twitter</span></a>
															</li>
															<li class="social-icon google"><a class="fa fa-google" href="#" title="Google"><span>Google</span></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
												
											
										
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 client-logo">
							<div class="client-logo-content">
								<h2 class="about-title">Our Happy Clients</h2>
								<div class="client-logo-slider">
									<div id="ytcs579c07146456674551469843220" class="yt-content-slider owl-theme yt-content-slider-style-1 arrow-default owl2-carousel owl2-theme owl2-loaded" data-autoplay="no" data-autoheight="no" data-delay="4" data-speed="0.6" data-margin="30" data-item_lg="4" data-item_sm="3" data-item_xs="1" data-arrows="yes" data-pagination="no" data-lazyload="yes" data-loop="no" data-hoverpause="no">
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<a href="#" title="Client"> <img src="image/demo/about/about-client-1.png" alt="Client"> </a>
											</div>
										</div>
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<a href="#" title="Client"> <img src="image/demo/about/about-client-2.png" alt="Client"> </a>
											</div>
										</div>
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<a href="#" title="Client"> <img src="image/demo/about/about-client-3.png" alt="Client"> </a>
											</div>
										</div>
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<a href="#" title="Client"> <img src="image/demo/about/about-client-4.png" alt="Client"> </a>
											</div>
										</div>
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<a href="#" title="Client"> <img src="image/demo/about/about-client-5.png" alt="Client"> </a>
											</div>
										</div>
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<a href="#" title="Client"> <img src="image/demo/about/about-client-6.png" alt="Client"> </a>
											</div>
										</div>
										<div class="yt-content-slide yt-clearfix yt-content-wrap">
											<div class="item">
												<a href="#" title="Client"> <img src="image/demo/about/about-client-1.png" alt="Client"> </a>
											</div>
										</div>
											
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- //Main Container -->

@include('frontend.includes.footer')
@include('frontend.includes.scripts')