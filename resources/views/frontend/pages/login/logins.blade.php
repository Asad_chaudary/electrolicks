@include('frontend.includes.head') 
@include('frontend.includes.header1')
<div class="main-container container">
		<ul class="breadcrumb">
			<li><a href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
			<li><a href="{{url('/')}}">Home</a></li>
			<li><a href="{{url('/logins')}}">Login</a></li>
		</ul>
		
		<div class="row">
			<div id="content" class="col-sm-12">
				<div class="page-login">
				
					<div class="account-border">
						<div class="row">
							<div class="col-sm-6 new-customer">
								<div class="well">
									<h2><i class="fa fa-file-o" aria-hidden="true"></i> New Customer</h2>
									<p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
								</div>
								<div class="bottom-form">
									<a href="{{url('/signup')}}" class="btn btn-default pull-right">Continue</a>
								</div>
							</div>
							
							<form action="#" method="post" enctype="multipart/form-data">
								<div class="col-sm-6 customer-login">
									<div class="well">
										<h2><i class="fa fa-file-text-o" aria-hidden="true"></i> Returning Customer</h2>
										<p><strong>I am a returning customer</strong></p>
										<div class="form-group">
											<label class="control-label " for="input-email">E-Mail</label>
											<input type="text" name="email" value="" id="input-email" class="form-control" placeholder="Type E-Mail - - -" />
										</div>
										<div class="form-group">
											<label class="control-label " for="input-password">Password</label>
											<input type="password" name="password" value="" id="input-password" class="form-control" placeholder="Type Password - - -" />
										</div>
									</div>
									<div class="bottom-form">
										<a href="#" class="forgot">Forgotten Password</a>
										<input type="submit" value="Login" class="btn btn-default pull-right" />
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
<style>
								.page-login .customer-login .well input {
										/* background: #f4f4f5; */
									    border: 1px solid #f4f4f5;
									    border-bottom: 2px solid #bcc9d1;
									    border-radius: 4px 4px 0 0;
									    padding:8px 12px ;
									    height: 48px;
									}

									.page-login .customer-login .well input:focus {
										box-shadow: none;
										/* border: none; */
									    border-bottom-color: #1d2124;
									    background: none;
									    border-left-color: #bcc9d1;
									    border-right-color: #bcc9d1;
									    border-top-color: #bcc9d1;
										/* background-color: rgba(0, 0, 0, 0.1); */
									}
							</style>
@include('frontend.includes.footer')
@include('frontend.includes.scripts')