<!----	Header center -->
	<div class="header-center left">
		<div class="container">
			<div class="row">
				<!-- Logo -->
				<div class="navbar-logo col-md-3 col-sm-12 col-xs-12">
					<a href="{{url('/')}}"><img src="{{ asset('frontend-assets') }}/image/demo/logos/electrologo.png" title="Electrolicks" alt="Electrolicks" style="height: 80px; position: relative; top: -20px;" /></a>
				</div>
				<!-- //end Logo -->

				<!-- Main Menu -->
				<div class="megamenu-hori navbar-menu col-lg-7 col-md-7 col-sm-6 col-xs-6">
					<div class="responsive so-megamenu ">
						<nav class="navbar-default">
							<div class=" container-megamenu  horizontal">
								
								<div class="navbar-header">
									<button type="button" id="show-megamenu" data-toggle="collapse" class="navbar-toggle">
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
									Navigation		
								</div>
								
								<div class="megamenu-wrapper">
									<span id="remove-megamenu" class="fa fa-times"></span>
									<div class="megamenu-pattern">
										<div class="container">
											<ul class="megamenu " data-transition="slide" data-animationtime="250">
												<li class="with-sub-menu hover">
													<a href="{{url('/')}}" style="color: black; font-weight: bold;">Home</a>
												</li>
													<li class="with-sub-menu hover">
													<a href="#" style="color: black; font-weight: bold;">
														Featured Products
													</a>
													</li>
													<li class="with-sub-menu hover">
													<a href="{{url('/faqs')}}" style="color: black; font-weight: bold;">
														FAQS
													</a>
													</li>
													<li class="with-sub-menu hover">
													<a href="{{url('/about')}}" style="color: black; font-weight: bold;">
														About Us
													</a>
													</li>
													<li class="with-sub-menu hover">
													<a href="{{url('/contact')}}" style="color: black; font-weight: bold;">
														Contact Us
													</a>
													</li>
												
													</li>
												
											</ul>
											
										</div>
									</div>
								</div>
							</div>
						</nav>
					</div>
				</div>
				<!-- //Main Menu -->

				<!-- Secondary menu -->
				<div class="col-md-2 col-sm-6 col-xs-6 shopping_cart pull-right">
					<!--cart-->
					<div id="cart" class=" btn-group btn-shopping-cart">
						<a data-loading-text="Loading..." class="top_cart dropdown-toggle" data-toggle="dropdown">
							<div class="shopcart">
								<span class="handle pull-left"></span>
								<span class="title">My cart</span>
								<p class="text-shopping-cart cart-total-full">2 item(s) - $1,262.00 </p>
							</div>
						</a>

						<ul class="tab-content content dropdown-menu pull-right shoppingcart-box" role="menu">
							<li>
								<table class="table table-striped">
									<tbody>
										<tr>
										<td class="text-center" style="width:70px">
											<a href="product.html"> <img src="{{ asset('frontend-assets') }}/image/demo/shop/product/resize/2.jpg" style="width:70px" alt="Filet Mign" title="Filet Mign" class="preview"> </a>
										</td>
										<td class="text-left"> <a class="cart_product_name" href="product.html">Filet Mign</a> </td>
										<td class="text-center"> x1 </td>
										<td class="text-center"> $1,202.00 </td>
										<td class="text-right">
											<a href="product.html" class="fa fa-edit"></a>
										</td>
										<td class="text-right">
											<a onclick="cart.remove('2');" class="fa fa-times fa-delete"></a>
										</td>
									</tr>
									<tr>
										<td class="text-center" style="width:70px">
											<a href="product.html"> <img src="{{ asset('frontend-assets') }}/image/demo/shop/product/resize/3.jpg" style="width:70px" alt="Canon EOS 5D" title="Canon EOS 5D" class="preview"> </a>
										</td>
										<td class="text-left"> <a class="cart_product_name" href="product.html">Canon EOS 5D</a> </td>
										<td class="text-center"> x1 </td>
										<td class="text-center"> $60.00 </td>
										<td class="text-right">
											<a href="product.html" class="fa fa-edit"></a>
										</td>
										<td class="text-right">
											<a onclick="cart.remove('1');" class="fa fa-times fa-delete"></a>
										</td>
									</tr>
									</tbody>
								</table>
							</li>
							<li>
								<div>
									<table class="table table-bordered">
										<tbody>
											<tr>
												<td class="text-left"><strong>Sub-Total</strong>
												</td>
												<td class="text-right">$1,060.00</td>
											</tr>
											<tr>
												<td class="text-left"><strong>Eco Tax (-2.00)</strong>
												</td>
												<td class="text-right">$2.00</td>
											</tr>
											<tr>
												<td class="text-left"><strong>VAT (20%)</strong>
												</td>
												<td class="text-right">$200.00</td>
											</tr>
											<tr>
												<td class="text-left"><strong>Total</strong>
												</td>
												<td class="text-right">$1,262.00</td>
											</tr>
										</tbody>
									</table>
									<p class="text-right"> <a class="btn view-cart" href="cart.html"><i class="fa fa-shopping-cart"></i>View Cart</a>&nbsp;&nbsp;&nbsp; <a class="btn btn-mega checkout-cart" href="checkout.html"><i class="fa fa-share"></i>Checkout</a> </p>
								</div>
							</li>
						</ul>
					</div>
					<!--//cart-->
				</div>
			</div>

		</div>
	</div>
	<!-- //Header center