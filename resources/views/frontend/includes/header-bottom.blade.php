<!-- Header Bottom -->
<div class="header-bottom compact-hidden">
   <div class="container">
      <div class="row">
         <div class="sidebar-menu col-md-3 col-sm-6 col-xs-8 ">
            <div class="responsive so-megamenu ">
               <div class="so-vertical-menu no-gutter compact-hidden">
                  <nav class="navbar-default">
                     <div class="container-megamenu vertical open">
                        <div id="menuHeading">
                           <div class="megamenuToogle-wrapper">
                              <div class="megamenuToogle-pattern">
                                 <div class="container">
                                    <div>
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                    </div>
                                    All Categories							
                                    <i class="fa pull-right arrow-circle fa-chevron-circle-up"></i>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="navbar-header">
                           <button type="button" id="show-verticalmenu" data-toggle="collapse" class="navbar-toggle fa fa-list-alt">
                           </button>
                           All Categories		
                        </div>
                        <div class="vertical-wrapper" >
                           <span id="remove-verticalmenu" class="fa fa-times"></span>
                           <div class="megamenu-pattern">
                              <div class="container">
                                 <ul class="megamenu">
                                 	@if(!empty(category()))
                                        @foreach(category() as $key => $value)
                                        <li class="item-vertical style1 with-sub-menu hover">
                                            <p class="close-menu"></p>
                                            <a href="#" class="clearfix"> 
                                                <img src="{{ asset('uploads/category/icons'.'/'.$value->c_icon) }}" alt="icon"> 
                                                <span> {{ $value->c_name }} </span>

                                                @if(isset($value->subcategory_type) && count($value->subcategory_type) > 0)
                                                <b class="caret"></b> 
                                                @endif
                                            </a>
                                          @if(isset($value->subcategory_type) && count($value->subcategory_type) > 0)
                                           <div class="sub-menu" data-subwidth="100">
                                              <div class="content">
                                                 <div class="row">
                                                    <div class="col-sm-12">
                                                       <div class="row">
                                                          <div class="col-md-4 static-menu">
                                                             <div class="menu">
                                                                <ul>
                                                                  @foreach($value->subcategory_type as $index => $subcat_type)
                                                                   <li>
                                                                      <a href="#" class="main-menu"> {{ $subcat_type['sct_name'] }} </a>
                                                                      @if(isset($subcat_type['subcategory']) && !empty($subcat_type['subcategory']))
                                                                        <ul>
                                                                          @foreach($subcat_type['subcategory'] as $i => $subcat)
                                                                           <li>
                                                                              <a href="#">
                                                                                {{ $subcat['sc_name'] }}
                                                                              </a>
                                                                            </li>
                                                                           @endforeach
                                                                       
                                                                        </ul>
                                                                      @endif
                                                                   </li>
                                                                   @endforeach
                                                                 
                                                                </ul>
                                                             </div>
                                                          </div>

                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                           @endif
                                        </li>
                                     @endforeach
                                    @endif
                                    <li class="item-vertical style1 with-sub-menu hover">
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/2.png" alt="icon">
                                       <span>Fans</span>
                                       </a>
                                       </li>
                                    <li class="item-vertical">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/10.png" alt="icon">
                                       <span>Appliances</span>
                                       </a>
                                    </li>
                                    <li class="item-vertical with-sub-menu hover">
                                       <a href="#" class="clearfix">
                                       <span class="label"></span>
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/3.png" alt="icon">
                                       <span>Wires & Flexibles</span>
                                       </a>
                                       </li>
                                    <li class="item-vertical with-sub-menu hover">
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/2.png" alt="icon">
                                       <span>Cables</span>
                                       </a>
                                       </li>
                                    <li class="item-vertical css-menu with-sub-menu hover">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/5.png" alt="icon">
                                       <span>Switchgears</span>
                                       </a>
                                       </li>
                                    <li class="item-vertical">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/11.png" alt="icon">
                                       <span>Motors & Dives</span>
                                       </a>
                                    </li>
                                    <li class="item-vertical">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/4.png" alt="icon">
                                       <span>Pumps</span>
                                       </a>
                                    </li>
                                    <li class="item-vertical">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/6.png" alt="icon">
                                       <span>Lighting</span>
                                       </a>
                                    </li>
                                    <li class="item-vertical">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/7.png" alt="icon">
                                       <span>Circuit Breakers</span>
                                       </a>
                                    </li>
                                    <li class="item-vertical">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/8.png" alt="icon">
                                       <span>Switching & Wiring Accessories</span>
                                       </a>
                                    </li>
                                     <li class="item-vertical" style="display: none;">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/8.png" alt="icon">
                                       <span>Pipes & Fittings </span>
                                       </a>
                                    </li>
                                    <li class="item-vertical" style="display: none;">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/12.png" alt="icon">
                                       <span>Meters & Capacitors</span>
                                       </a>
                                    </li>
                                    <li class="item-vertical" style="display: none;">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/13.png" alt="icon">
                                       <span>Cable Trays, Jointing Kits & Accessories</span>
                                       </a>
                                    </li>
                                    <li class="item-vertical" style="display: none;">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/13.png" alt="icon">
                                       <span>Stabilizers, Invertors, UPS & Batteries</span>
                                       </a>
                                    </li>
                                    <li class="item-vertical" style="display: none;">
                                       <p class="close-menu"></p>
                                       <a href="#" class="clearfix">
                                       <img src="{{ asset('frontend-assets') }}/image/theme/icons/13.png" alt="icon">
                                       <span>Sanitisation Material</span>
                                       </a>
                                    </li>
                                    <li class="loadmore">
                                       <i class="fa fa-plus-square-o"></i>
                                       <span class="more-view">More Categoriesx</span>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </nav>
               </div>
            </div>
         </div>
         <!-- Search -->
         <div class="header-bottom-right  col-md-9 col-sm-6 col-xs-4 ">
            <div id="sosearchpro" class="col-sm-7 search-pro">
               <form method="GET" action="https://demo.smartaddons.com/templates/html/market/index.html">
                  <div id="search0" class="search input-group">
                     <div class="select_category filter_type icon-select">
                        <select class="no-border" name="category_id">
                           <option value="0">All Categories  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                           @if(!empty(category()))
                           @foreach(category() as $key => $value)
                            <option value="{{ $value->id }}"> {{ $value->c_name }} </option>
                           @endforeach
                           @endif
                           
                        </select>
                     </div>
                     <input class="autosearch-input form-control" type="text" value="" size="50" autocomplete="off" placeholder="Search" name="search">
                     <span class="input-group-btn">
                     <button type="submit" class="button-search btn btn-primary" name="submit_search"><i class="fa fa-search"></i></button>
                     </span>
                  </div>
                  <input type="hidden" name="route" value="product/search" />
               </form>
            </div>
         </div>
         <!-- //end Search -->
      </div>
   </div>
</div>