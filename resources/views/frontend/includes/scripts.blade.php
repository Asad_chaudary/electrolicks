	
<!-- Include Libs & Plugins
============================================ -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/themejs/libs.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/unveil/jquery.unveil.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/countdown/jquery.countdown.min.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/datetimepicker/moment.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/modernizr/modernizr-2.6.2.min.js"></script>


<!-- Theme files
============================================ -->
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/themejs/application.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/themejs/homepage.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/themejs/so_megamenu.js"></script>
<script type="text/javascript" src="{{ asset('frontend-assets') }}/js/themejs/addtocart.js"></script>	

<script type="text/javascript">
	$.ajaxSetup({
    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
</script>

@stack('js')
	
</body>

</html>